#version 0.2 docker file for teams-jasper-workers

FROM ubuntu:14.04
MAINTAINER dmiguel@ptsteams.com

RUN apt-get update && apt-get -y install python2.7 python-pip 

RUN mkdir -p /home/modules/jasper   && \
    mkdir /home/modules/shared

ADD amqp /home/amqp 
ADD jasper-workers /home/jasper-workers
ADD modules/jasperMessageWorkerWrapper.py /home/modules/jasperMessageWorkerWrapper.py 
ADD modules/jasper/* /home/modules/jasper/
ADD modules/shared/* /home/modules/shared/

RUN pip install requests==2.2.1 && \
    pip install argcomplete==1.0.0 && \
    pip install pyLogger==1.0 && \
    pip install colorlog==2.6.0 && \
    pip install pika==0.10.0

RUN echo "America/Chicago" | tee /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime

WORKDIR /home/modules/

CMD ["python", "/home/modules/jasperMessageWorkerWrapper.py"]
