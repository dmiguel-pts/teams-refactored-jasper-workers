#!/usr/local/lib/teams/python/bin/python
import os
import sys
import traceback
import uuid
import logging
import ConfigParser
import argparse
import pika
import pika.exceptions

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/amqp")
print("reading amqp message ****************")
try:
    pythonLibRoot = config.get("global", "pythonLibRoot")
except ConfigParser.Error as error:
    sys.exit("Configuration error:\n{0}".format(error))
else:
    sys.path.append(pythonLibRoot)

try:
    from shared import pyLogger
except ImportError as error:
    sys.exit("Unable to import the 'pyLogger' module.  Check that it exists under {0}/shared.".format(pythonLibRoot))
try:
    pyLogger.makeLogger("amqp")
except pyLogger.LoggingException as error:
    sys.exit("Unable to initialize the logger:\n{0}".format(error))
logging.getLogger("pika").setLevel("ERROR")
for handler in logging.getLogger("amqp").handlers:
    if handler.name == "fileHandler":
        logging.getLogger("pika").addHandler(handler)

try:
    defaultTimeout = int(config.get("amqp", "makerConnectionTimeout"))
except ConfigParser.Error as error:
    defaultTimeout = 0
    logging.getLogger("amqp").warn("Default connection timeout could not be derived from the configuration - assuming that it will be passed as an argument.")
try:
    defaultAmqpUser = config.get("amqp", "amqpUser")
except ConfigParser.Error as error:
    defaultAmqpUser = None
    logging.getLogger("amqp").warn("Default AMQP username could not be derived from the configuration - assuming that it will be passed as an argument.")
try:
    defaultAmqpPassword = config.get("amqp", "amqpPassword")
except ConfigParser.Error as error:
    defaultAmqpPassword = None
    logging.getLogger("amqp").warn("Default AMQP password could not be derived from the configuration - assuming that it will be passed as an argument.")
try:
    defaultAmqpAddress = config.get("amqp", "amqpHost")
except ConfigParser.Error as error:
    defaultAmqpAddress = None
    logging.getLogger("amqp").warn("Default AMQP address could not be derived from the configuration - assuming that it will be passed as an argument.")

class MessagingException(Exception):
    def __init__(self, message):
        self.message = message
        logging.getLogger("amqp").debug(self.message)
    def __str__(self):
        return repr(self.message)

class MessageMaker():
    def __init__(self, exchangeName, timeout=defaultTimeout, user=defaultAmqpUser, password=defaultAmqpPassword, address=defaultAmqpAddress):
        if not exchangeName:
            raise MessagingException("An exchange name is required.")
        self.exchangeName = exchangeName
        self.timeout = int(timeout)
        self.timeoutId = None
        self.amqpURL = "amqp://{0}:{1}@{2}:5672/%2F".format(user, password, address)
        self.connect()
        
    def connect(self):
        logging.getLogger("amqp").debug("Connecting to {0}".format(self.amqpURL))
        try:
            self.connection = pika.BlockingConnection(
                pika.URLParameters(self.amqpURL)
            )
        except (pika.exceptions.AuthenticationError, pika.exceptions.ProbableAuthenticationError) as error:
            raise MessagingException("AMQP Authentication error:\n{0}".format(error))
        except pika.exceptions.AMQPConnectionError as error:
            raise MessagingException("Failed to connect to the AMQP server:\n{0}".format(error))
        self.channel = self.connection.channel()
        self.channel.exchange_declare(
            exchange = self.exchangeName,
            exchange_type = "topic"
        )
    
    def onRpcResponse(self, channel, method, properties, body):
        if not all([channel, method, properties, body]):
            raise MessagingException("The 'onRpcResponse' function was called with null arguments.")
        logging.getLogger("amqp").debug("Received a response. Correlation ID is:\n{0}\nBody is:\n{1}".format(
            properties.correlation_id,
            body
        ))
        if self.correlationId == properties.correlation_id:
            if self.rpcCallback:
                logging.getLogger("amqp").debug("An RPC callback has been provided, so it is being called with the message body as the argument")
                self.rpcCallback(body)
            else:
                logging.getLogger("amqp").debug("An RPC callback has NOT been provided, so appending the message body to a string to return to the caller.")
                if self.response:
                    self.response = "{0}\n{1}".format(self.response, body)
                else:
                    self.response = body
            if self.expectedResponses:
                self.responseCount = self.responseCount + 1
                logging.getLogger("amqp").debug("The current response count is: {0}".format(self.responseCount))
    
    def onConnectionTimeout(self):
        if self.rpc and self.messageSent:
            if self.expectedResponses:
                self.responseCount = self.responseCount + 1
                if self.responseCount < self.expectedResponses:
                    logging.getLogger("amqp").debug("The connection timeout has been reached, but since not all responses have been received, the response count will incremented, and the timeout reset.")
                    logging.getLogger("amqp").debug("The current response count is: {0}".format(self.responseCount))
                    logging.getLogger("amqp").debug("Removing the connection timeout.")
                    self.connection.remove_timeout(self.timeoutId)
                    logging.getLogger("amqp").debug("Creating a new connection timeout of '{0}'.".format(self.timeout))
                    self.timeoutId = self.connection.add_timeout(self.timeout, self.onConnectionTimeout)
                else:
                    raise MessagingException("The AMQP connection timed out before the expected number of responses were received.")
            else:
                logging.getLogger("amqp").debug("The connection timeout has been reached.")
                self.untilTimeout = False
        else:
            raise MessagingException("The AMQP connection timed out before the message could be sent to the exchange.")
    
    def send(self, routingKey, message, rpc=False, expectedResponses=0, correlationId=None, rpcCallback=None):
        if not routingKey:
            raise MessagingException("The send method was called with a null routingKey.")
        self.routingKey = routingKey
        self.message = message
        self.rpc = rpc
        self.expectedResponses = expectedResponses
        self.correlationId = correlationId
        self.messageSent = False
        self.responseQueue = None
        self.responseCount = 0
        self.response = ""
        self.rpcCallback = rpcCallback
        
        logging.getLogger("amqp").debug("The 'send' method of MessageMaker was called.")
        logging.getLogger("amqp").debug("Setting a connection timeout of '{0}'.".format(self.timeout))
        self.timeoutId = self.connection.add_timeout(self.timeout, self.onConnectionTimeout)
        if self.rpc is True:
            if self.expectedResponses:
                logging.getLogger("amqp").debug("Expecting {0} responses.".format(expectedResponses))
            else:
                logging.getLogger("amqp").debug("Waiting {0} seconds for any responses.".format(self.timeout))
                
            if not self.correlationId:
                self.correlationId = str(uuid.uuid4())
            self.responseCount = 0
            self.response = ""
            
            result = self.channel.queue_declare(
                exclusive=True
            )
            self.responseQueue = result.method.queue
            self.channel.queue_bind(
                queue = self.responseQueue,
                exchange = "teams"
            )
            
            logging.getLogger("amqp").debug("Consuming the {0} queue for the return message.".format(self.responseQueue))
            self.channel.basic_consume(
                consumer_callback = self.onRpcResponse,
                no_ack = True,
                queue = self.responseQueue
            )
            logging.getLogger("amqp").debug("Publishing a message with a routing key of '{0}'.  This is an RPC request with a correlation ID of '{1}'.".format(
                self.routingKey,
                self.correlationId
            ))
            self.channel.basic_publish(
                exchange = self.exchangeName,
                routing_key = self.routingKey,
                properties = pika.BasicProperties(
                    reply_to = self.responseQueue,
                    correlation_id = self.correlationId,
                    delivery_mode = 1
                ),
                body = self.message
            )
            self.messageSent = True
            self.untilTimeout = True
            if self.expectedResponses:
                while self.responseCount < self.expectedResponses:
                    self.connection.process_data_events()
            else:
                while self.untilTimeout:
                    self.connection.process_data_events()
            logging.getLogger("amqp").debug("Removing the connection timeout.")
            self.connection.remove_timeout(self.timeoutId)
            return self.response
        else:
            if self.expectedResponses:
                raise MessagingException("MessageMaker was initialized as sending a one way message, but expectedResponses is set to greater than 0.")
            if correlationId:
                logging.getLogger("amqp").debug("Publishing a message with a routing key of '{0}'.  Using a correlation ID of: '{1}'".format(
                    self.routingKey,
                    correlationId
                ))
                self.channel.basic_publish(
                    exchange = self.exchangeName,
                    routing_key = self.routingKey,
                    properties = pika.BasicProperties(
                        correlation_id = self.correlationId
                    ),
                    body = self.message
                )
            else:
                logging.getLogger("amqp").debug("Publishing a message with a routing key of '{0}'".format(self.routingKey))
                self.channel.basic_publish(
                    exchange = self.exchangeName,
                    routing_key = self.routingKey,
                    body = self.message
                )
            logging.getLogger("amqp").debug("Removing the connection timeout.")
            self.connection.remove_timeout(self.timeoutId)
            return "Message has been published"
    
    def close(self):
        if self.channel.is_open:
            logging.getLogger("amqp").debug("Closing channel.")
            self.channel.basic_cancel(self)
        if self.connection.is_open:
            logging.getLogger("amqp").debug("Closing connection.")
            self.connection.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = """A generic message maker designed to be imported by other modules that need to send messages.
It can also be called from the command line to, for example, manually test message consumers without needing producer logic.""")
    parser.add_argument(
        "routingKey",
        help = """The routing key determines which queues will recieve the message.
Wild cards and dot notation is allowed, and a # key will produce a message that will be consumed by every queue bound to the exchange.
For example, a message with key "ARLI.deploy" would be consumed by a queue bound to any of the following keys:
ARLI, ARLI.*, ARLI.deploy, ARLI.deploy.01, #"""
    )
    parser.add_argument(
        "message",
        help = """The string that will form the body of the message.
The nature of this string depends on the nature of the process that will consume it.  For example, it could be a smple word or a JSON object."""
    )
    parser.add_argument(
        "-e",
        "--exchange",
        dest = "exchangeName",
        default = "teams",
        help = """The name of the exchange to be declared.
A single topic exchange is all that is needed in most cases, but multiple exchanges may be needed if using the fanout topic (#).
(default: %(default)s)"""
    )
    parser.add_argument(
        "-r",
        "--rpc",
        action = "store_true",
        help = """Include this option if the message requires a response from its consumers.
Responses will be listened for on a randomly created queue bound to the 'rpc' exchange.
Use the --count option to specify how many responses are expected.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-c",
        "--count",
        dest = "expectedResponses",
        default = 0,
        type = int,
        help = """When producing an RPC message, the numerical argument to option specifies how many responses are expected (i.e. how long to wait).
This option is meaningless unless the --rpc option is also specified.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-t",
        "--timeout",
        dest = "timeout",
        default = defaultTimeout,
        type = int,
        help = """The amount of time in seconds to wait for the AMQP server to respond, before performing an appropriate action.
If publishing a message, the attempt will be cancelled after this many seconds.
If waiting for an RPC to return, the response count will be incremented by one after this many seconds (i.e. each responder has this many seconds to respond).
(default: %(default)s)"""
    )
    parser.add_argument(
        "-i",
        "--correlationId",
        dest = "correlationId",
        default = str(uuid.uuid4()),
        help = """When the message is involved in RPC, either as the initial message or the response, the correlation ID must be the same on both sides.
If a correlation ID is not specified, and the --rpc option is specified, a random correlation ID will be generated.  
(default: %(default)s)"""
    )
    parser.add_argument(
        "-u",
        "--user",
        default = defaultAmqpUser,
        help = """The username to use when authenticating with the RabbitMQ server.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-p",
        "--password",
        default = defaultAmqpPassword,
        help = """The password to use when authenticating with the RabbitMQ server.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-a",
        "--address",
        default = defaultAmqpAddress,
        help = """The address of the RabbitMQ server.
(default: %(default)s)"""
    )
    
    args = parser.parse_args()
    routingKey = args.routingKey
    exchangeName = args.exchangeName
    message = args.message
    rpc = args.rpc
    expectedResponses = args.expectedResponses
    correlationId = args.correlationId
    timeout = args.timeout
    user = args.user
    password = args.password
    address = args.address
    
    logging.getLogger("amqp").debug("""Creating a message with the following parameters:
routing key: {0}
message: {1}
exchange name: {2}
rpc: {3}
expected responses: {4}
timeout: {5}
correlation ID: {6}
AMQP URL: {7}""".format(
        routingKey,
        message,
        exchangeName,
        rpc,
        expectedResponses,
        timeout,
        correlationId,
        "amqp://{0}:{1}@{2}:5672/%2F".format(user, password, address)
    ))
    
    def parserRpcCallback(body):
        print body
    
    try:
        messageMaker = MessageMaker(exchangeName, timeout, user, password, address)
        print messageMaker.send(routingKey, message, rpc, expectedResponses, correlationId, rpcCallback=parserRpcCallback)
    except (pika.exceptions.AuthenticationError, pika.exceptions.ProbableAuthenticationError) as error:
        logging.getLogger("amqp").error("{0} - AMQP Authentication error:\n{1}".format(routingKey, error))
    except pika.exceptions.AMQPConnectionError as error:
        logging.getLogger("amqp").error("{0} - Failed to connect to the AMQP server:\n{1}".format(routingKey, error))
    except MessagingException as error:
        logging.getLogger("amqp").error("{0} - Encountered a messaging related error:\n{1}".format(routingKey, error))
    except Exception as error:
        logging.getLogger("amqp").exception("{0} - Encountered an unexpected error:\n{1}".format(routingKey, traceback.format_exc()))
    finally:
        try:
            messageMaker.close()
        except NameError as error:
            pass
