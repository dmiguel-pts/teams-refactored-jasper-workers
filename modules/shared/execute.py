import sys
import subprocess
import logging

class ExecuteException(Exception):
    def __init__(self, returncode, output):
        self.returncode = returncode
        self.output = output
    def __str__(self):
        return repr(self.output)

def execute(command, loggerName=None, logLevel=logging.DEBUG, output=True):
    if isinstance(logLevel, str):
        logLevel = getattr(logging, logLevel.upper(), None)
    if not isinstance(logLevel, int):
        raise ExecuteException("Invalid log level")
    
    if loggerName:
        logging.getLogger(loggerName).debug(command)
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    returnString = ""
    thisLine = ""
    while True:
        nextChar = process.stdout.read(1)
        if nextChar == '' and process.poll() is not None:
            break
        if nextChar == "\n":
            if loggerName:
                logging.getLogger(loggerName).log(logLevel, thisLine)
            thisLine = ""
        else:
            thisLine = thisLine + nextChar
        if output:
            sys.stdout.write(nextChar)
        returnString = returnString + nextChar
    
    returncode = process.returncode
    if (returncode == 0):
        return returnString.strip("\n")
    else:
        raise ExecuteException(returncode, returnString)
