import os
import sys
import errno
import logging
import logging.handlers
import ConfigParser
import colorlog

class LoggingException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

def makeLogger(loggerName, configPath=None, logPath=None, logLevel=None, output=True):
    if not configPath:
        configPath = "/home/{0}".format(loggerName)
    config = ConfigParser.SafeConfigParser(os.environ)
    config.read(configPath)
    
    if not logPath:
        try:
            logRoot = config.get("logging", "logRoot")
        except ConfigParser.Error as error:
            logRoot = "/var/log/{0}".format(loggerName)
        finally:
            logPath = os.path.join(logRoot, "{0}.log".format(loggerName))
    
    
    if not os.path.isdir(os.path.dirname(logPath)):
        try:
            os.makedirs(os.path.dirname(logPath))
        except EnvironmentError as error:
            if error.errno != errno.EEXIST:
                print "Unable to create the {0} directory:\n{1}".format(
                    os.path.dirname(logPath),
                    error
                )
    
    if not logLevel:
        try:
            logLevel = config.get("logging", "logLevel")
        except ConfigParser.Error as error:
            logLevel = "DEBUG"
    for handler in logging.getLogger(loggerName).handlers:
        logging.getLogger(loggerName).removeHandler(handler)
    
    
    logging.OUTPUT = 15
    logging.addLevelName(15, "OUTPUT")
    logging.Logger.output = lambda inst, msg, *args, **kwargs: inst.log(logging.OUTPUT, msg, *args, **kwargs)
    logging.getLogger().setLevel(logging.DEBUG)
    logging.getLogger(loggerName).setLevel(logging.DEBUG)
    
    outputHandler = logging.StreamHandler(sys.stdout)
    outputHandler.name = "outputHandler"
    outputHandler.setFormatter(colorlog.ColoredFormatter(
        "%(log_color)s%(message)s",
        log_colors = {
            "DEBUG": "white",
            "WARNING": "yellow",
            "ERROR": "red"
        }
    ))
    
    try:
        fileHandler = logging.handlers.RotatingFileHandler(
            filename = logPath,
            maxBytes = 5242880,
            backupCount = 5
        )
    except IOError as error:
        outputHandler.setLevel(logging.getLevelName(logLevel))
    else:
        outputHandler.setLevel(logging.OUTPUT)
        fileHandler.name = "fileHandler"
        fileHandler.setLevel(logging.getLevelName(logLevel))
        fileHandler.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(message)s"))
        logging.getLogger(loggerName).addHandler(fileHandler)
    finally:
        if output:
            logging.getLogger(loggerName).addHandler(outputHandler)
