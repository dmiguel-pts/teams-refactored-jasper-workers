#!/usr/local/lib/teams/python/bin/python
import importlib
import sys
import os
import traceback
import argparse
import time
import logging
import multiprocessing
import atexit
import ConfigParser
import pika

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/amqp")

try:
    pythonLibRoot = config.get("global", "pythonLibRoot")
    sys.path.append(pythonLibRoot)

    from shared import pyLogger
        
    pyLogger.makeLogger("amqp")
    logging.getLogger("pika").setLevel("ERROR")

    for handler in logging.getLogger("amqp").handlers:
        if handler.name == "fileHandler":
            logging.getLogger("pika").addHandler(handler)

    defaultReconnectWait = int(config.get("amqp", "workerReconnectWait"))
    defaultProcPool = int(config.get("amqp", "procPool"))
    defaultAmqpUser = config.get("amqp", "amqpUser")
    defaultAmqpPassword = config.get("amqp", "amqpPassword")
    defaultAmqpAddress = config.get("amqp", "amqpHost")
    defaultAmqpPort = config.get("amqp", "amqpPort")
except:
    logging.getLogger("amqp").error("Unable to read and set all fields from the config file");

class MessagingException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

class ConsumerDetails:
    def __init__(self, queueName, routingKey, messageHandlerName, exchangeName="teams"):
        if not queueName:
            raise MessagingException("A MessageWorker.ConsumerDetails object was instantiated without a valid queue name.")
        if not routingKey:
            raise MessagingException("A MessageWorker.ConsumerDetails object was instantiated without a valid routing key.")
        if not messageHandlerName:
            raise MessagingException("A MessageWorker.ConsumerDetails object was instantiated without a valid message handler name.")
        self.messageHandlerName = messageHandlerName
        self.exchangeName = exchangeName
        self.queueName = queueName
        self.routingKeys = list()
        self.consumerTags = list()
        self.bindingCount = 0
        
        for i in range(len(routingKey.split("."))):
            self.routingKeys.append(routingKey.rsplit(".",i)[0])

class MessageWorker:
    def __init__(self, *consumerDetailsTuple, **kwargs):
        try:
            user = kwargs["user"]
        except LookupError as error:
            user = defaultAmqpUser
        try:
            password = kwargs["password"]
        except LookupError as error:
            password = defaultAmqpPassword
        try:
            address = kwargs["address"]
        except LookupError as error:
            address = defaultAmqpAddress
        try:
            port = kwargs["port"]
        except LookupError as error:
            port = defaultAmqpPort
        try:
            reconnectWait = kwargs["reconnectWait"]
        except LookupError as error:
            reconnectWait = defaultReconnectWait
        try:
            procPool = kwargs["procPool"]
        except LookupError as error:
            procPool = defaultProcPool
        
        if procPool > 0:
            self.procPool = multiprocessing.Pool(processes=procPool)
        else:
            self.procPool = None
        
        self.consumerDetailsTuple = consumerDetailsTuple
        self.connection = None
        self.channel = None
        self.reconnectWait = reconnectWait
        self.amqpURL = "amqp://{0}:{1}@{2}:{3}/%2F".format(user, password, address, port)
        for consumerDetails in self.consumerDetailsTuple:
            if not isinstance(consumerDetails, ConsumerDetails):
                raise MessagingException("The MessageWorker constructor must be passed instances of ConsumerDetails")
            self.addOnExchangeDeclare(consumerDetails)
            self.addOnExchangeDeclare(consumerDetails)
            self.addOnQueueDeclare(consumerDetails)
            self.addOnBind(consumerDetails)
            self.addOnConsume(consumerDetails)
        
        @atexit.register
        def cleanup():
            self.stop()
    
    def start(self):
        logging.getLogger("amqp").debug("Creating connection and starting ioloop")
        while not isinstance(self.connection, pika.connection.Connection):
            try:
                self.connection = self.connect()
            except pika.exceptions.AMQPConnectionError as error:
                logging.getLogger("amqp").warning("Connection could not be opened: {0}".format(error))
                logging.getLogger("amqp").warning("Retrying connection in {0} second(s)".format(self.reconnectWait))
                time.sleep(self.reconnectWait)
            else:
                self.connection.ioloop.start()
    
    def stop(self):
        logging.getLogger("amqp").debug("Stop method was called - cleaning up")
        for consumerDetails in self.consumerDetailsTuple:
            for consumerTag in consumerDetails.consumerTags:
                self.channel.basic_cancel(
                    callback = self.onConsumerCancelled,
                    consumer_tag = consumerTag
                )
        self.connection.ioloop.start()
    
    def connect(self):
        logging.getLogger("amqp").debug("Connecting to '{0}'".format(self.amqpURL))
        return pika.SelectConnection(
            parameters = pika.URLParameters(self.amqpURL),
            on_open_callback = self.onConnectionOpen
         )
    
    def onConnectionOpen(self, frame):
        logging.getLogger("amqp").debug("Connection opened to '{0}'".format(self.amqpURL))
        self.connection.add_on_close_callback(self.onConnectionClosed)
        self.connection.channel(
            on_open_callback = self.onChannelOpen
        )
    
    def onConnectionClosed(self, *args):
        logging.getLogger("amqp").warning("Connection has been closed - attempting to reconnect")
        self.channel = None
        while self.connection.is_closed:
            try:
                self.connection = self.connect()
            except pika.exceptions.AMQPConnectionError as error:
                logging.getLogger("amqp").warning("Connection could not be opened: {0}".format(error))
                logging.getLogger("amqp").warning("Retrying connection in {0} second(s)".format(self.reconnectWait))
                time.sleep(self.reconnectWait)
    
    def onChannelOpen(self, channel):
        logging.getLogger("amqp").debug("Channel opened")
        self.channel = channel
        self.channel.add_on_close_callback(self.onChannelClosed)
        for consumerDetails in self.consumerDetailsTuple:
            self.channel.exchange_declare(
                callback = getattr(self, "onExchangeDeclare_{0}".format(consumerDetails.queueName)),
                exchange = consumerDetails.exchangeName,
                exchange_type = "topic"
            )
    
    def onChannelClosed(self, *args):
        logging.getLogger("amqp").warning("Channel has been closed - closing connection")
        self.connection.close()
    
    @classmethod
    def addOnExchangeDeclare(cls, consumerDetails):
        def onExchangeDeclare(self, frame):
            logging.getLogger("amqp").debug("Exchange '{0}' has been declared".format(consumerDetails.exchangeName))
            self.channel.queue_declare(
                queue = consumerDetails.queueName,
                callback = getattr(self, "onQueueDeclare_{0}".format(consumerDetails.queueName))
            )
        setattr(cls, "onExchangeDeclare_{0}".format(consumerDetails.queueName), onExchangeDeclare)
    
    @classmethod
    def addOnQueueDeclare(cls, consumerDetails):
        def onQueueDeclare(self, frame):
            logging.getLogger("amqp").debug("Queue '{0}' has been declared".format(consumerDetails.queueName))
            consumerDetails.bindingCount = 0
            for routingKey in consumerDetails.routingKeys:
                logging.getLogger("amqp").debug("Binding queue '{0}' to exchange '{1}' with routing key '{2}'.".format(
                    consumerDetails.queueName,
                    consumerDetails.exchangeName,
                    routingKey
                ))
                self.channel.queue_bind(
                    callback = getattr(self, "onBind_{0}".format(consumerDetails.queueName)),
                    queue = consumerDetails.queueName,
                    exchange = consumerDetails.exchangeName,
                    routing_key = routingKey
                )
        setattr(cls, "onQueueDeclare_{0}".format(consumerDetails.queueName), onQueueDeclare)
    
    @classmethod
    def addOnBind(cls, consumerDetails):
        def onBind(self, frame):
            consumerDetails.bindingCount = consumerDetails.bindingCount + 1
            if consumerDetails.bindingCount == len(consumerDetails.routingKeys):
                logging.getLogger("amqp").debug("Bindings complete on queue '{0}' - beginning consumption.".format(
                    consumerDetails.queueName
                ))
                self.channel.add_on_cancel_callback(self.onConsumerCancelled)
                consumerDetails.consumerTags.append(
                    self.channel.basic_consume(
                        consumer_callback = getattr(self, "onConsume_{0}".format(consumerDetails.queueName)),
                        queue = consumerDetails.queueName
                    )
                )
        setattr(cls, "onBind_{0}".format(consumerDetails.queueName), onBind)
    
    @classmethod
    def addOnConsume(cls, consumerDetails):
        def onConsume(self, *args, **kwargs):
            logging.getLogger("amqp").debug("Received a message for consumer '{0}'.".format(consumerDetails.queueName))
            
            def formatArgs(channel, delivery, properties, *args): 
                formattedArgs = [properties.reply_to, properties.correlation_id]
                formattedArgs.extend(list(args))
                return delivery, formattedArgs
            delivery, args = formatArgs(*args)
            self.channel.basic_ack(delivery.delivery_tag)
            
            try:
                module = importlib.import_module(consumerDetails.messageHandlerName)
            except ImportError as error:
                raise MessagingException("Unable to import the {0} module:\n{1}".format(
                    consumerDetails.messageHandlerName,
                    error
                ))
            
            logging.getLogger("amqp").debug("Passing these arguments to the function '{0}.messageHandler':\n{1}".format(
                consumerDetails.messageHandlerName,
                args
            ))
            if self.procPool:
                self.procPool.apply_async(module.messageHandler, args)
            else:
                module.messageHandler(*args)
            
        setattr(cls, "onConsume_{0}".format(consumerDetails.queueName), onConsume)
    
    def onConsumerCancelled(self, frame):
        logging.getLogger("amqp").debug("The consumer with tag '{0}' has been cancelled.".format(frame.method.consumer_tag))
        if not [tag for tag in self.channel.consumer_tags if tag != frame.method.consumer_tag]:
            logging.getLogger("amqp").debug("The last consumer has been cancelled - closing the channel.")
            self.channel.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = """A generic message worker for TEAMS packages.  
This module expects a python-style configuration file at "/etc/teams/amqp".""")
    parser.add_argument(
        "routingKey",
        help = """The routing key to bind the queue to.
The consumer will consume any messages sent to the exchange matching this key.
Wild cards and dot notation are allowed, and a # key will consume every message sent to the exchange.
For example, a message with key "PROL.deploy" would be consumed by a queue bound to any of the following keys:
PROL, PROL.*, PROL.deploy, PROL.deploy.01, #"""
    )
    parser.add_argument(
        "-e",
        "--exchange",
        dest = "exchangeName",
        default = "teams",
        help = """The name of the exchange to be declared.
A single topic exchange is all that is needed in most cases, but multiple exchanges may be needed if using the fanout topic (#).
(default: %(default)s)"""
    )
    parser.add_argument(
        "-q",
        "--queue",
        dest = "queueName",
        help = """The name of the queue to be declared.
Queues are bound to routing keys, so by default, the routing key argument is used as the queue name.
Specify a unique queue name if multiple consumers bound to the same routing key each need their own queue, so they do not consume messages meant for each other.
Examples: 'reportBuild', 'deploy-01'
(default: same as routingKey)"""
    )
    parser.add_argument(
        "-m",
        "--handler",
        dest = "messageHandlerName",
        help = """The package and module containing a "messageHandler" function to call on consuming a message.
The package (i.e. directory) containing the module with the "messageHandler" function must be an immediate child of the path specified by the value of "pythonLibRoot" in the configuration file.  
If the routing key is composed of two words separated by a dot, and no message handler is specified with this option, this module will, by default, use the first word as the package and the second word as the module.
Examples: 'tomcat.deploy', 'general.status'
(default: same as routingKey)"""
    )
    parser.add_argument(
        "-r",
        "--reconnect",
        dest = "reconnectWait",
        default = defaultReconnectWait,
        type = int,
        help = """The amount of time in seconds to wait, if the connection to the AMQP server is closed, before attempting to connect again.
There may be firewalls or timeouts that occasionaly interrupt a constant connection.  This value can help accomodate those.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-u",
        "--user",
        default = defaultAmqpUser,
        help = """The username to use when authenticating with the RabbitMQ server.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-p",
        "--password",
        default = defaultAmqpPassword,
        help = """The password to use when authenticating with the RabbitMQ server.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-a",
        "--address",
        default = defaultAmqpAddress,
        help = """The address of the RabbitMQ server.
(default: %(default)s)"""
    )
    parser.add_argument(
        "-c",
        "--processors",
        dest = "procPool",
        default = defaultProcPool,
        type = int,
        help = """The number of subprocess workers to keep in a pool for consumers to use.
(default: %(default)s)"""
    )
    args = parser.parse_args()
    
    routingKey = args.routingKey
    exchangeName = args.exchangeName
    if args.queueName:
        queueName = args.queueName
    else:
        queueName = routingKey
    if args.messageHandlerName:
        messageHandlerName = args.messageHandlerName
    else:
        messageHandlerName = routingKey
    reconnectWait = args.reconnectWait
    procPool = args.procPool
    user = args.user
    password = args.password
    address = args.address
    
    logging.getLogger("amqp").debug("""Creating ConsumerDetails object with the following parameters:
queue: {0}
routing key: {1}
message handler: {2}
exchange: {3}""".format(
        queueName,
        routingKey,
        messageHandlerName,
        exchangeName
    ))
    
    try:
        consumerDetails = ConsumerDetails(queueName, routingKey, messageHandlerName, exchangeName)
    except MessagingException as error:
        logging.getLogger("amqp").error("Unable to create the ConsumerDetails object:\n{0}".format(error))
    else:
    
        logging.getLogger("amqp").debug("""Creating MessageWorker object with the following parameters:
    reconnect delay: {0}
    AMQP URL: {1}""".format(
            reconnectWait,
            "amqp://{0}:{1}@{2}:5672/%2F".format(user, password, address)
        ))
        
        try:
            worker = MessageWorker(consumerDetails, user=user, password=password, address=address, reconnectWait=reconnectWait, procPool=procPool)
            worker.start()
        except KeyboardInterrupt:
            logging.getLogger("amqp").warn("The 'message-worker' module received an interrupt signal.")
            worker.stop()
        except pika.exceptions.AMQPConnectionError as error:
            logging.getLogger("amqp").error("Unable to connect to the AMQP server:\n{0}".format(error))
        except (pika.exceptions.AuthenticationError, pika.exceptions.ProbableAuthenticationError) as error:
            logging.getLogger("amqp").error("An AMQP Authentication error occurred:\n{0}".format(error))
        except MessagingException as error:
            logging.getLogger("amqp").error("A messaging related error occurred:\n{0}".format(error))
        except Exception as error:
            logging.getLogger("amqp").exception("An unexpected error occurred:\n{0}".format(traceback.format_exc()))
    

