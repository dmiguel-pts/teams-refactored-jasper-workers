import sys
import os
import json
import logging
import re
import ConfigParser
import requests

class TopicException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
    jasperUser = config.get("rest", "jasperUser")
    jasperPassword = config.get("rest", "jasperPassword")
except ConfigParser.Error as error:
    raise TopicException("Configuration error:\n{0}".format(error))

authTuple = (jasperUser, jasperPassword)

