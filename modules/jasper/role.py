import sys
import os
import json
import logging
import re
import ConfigParser
import requests

class RoleException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
    authTuple = (
        config.get("rest", "jasperUser"),
        config.get("rest", "jasperPassword")
    )
except ConfigParser.Error as error:
    raise RoleException("Configuration error:\n{0}".format(error))

def get(orgId):
    logging.getLogger("rest").debug("Retrieving roles for organization '{0}'.".format(orgId))
    roleIdList = list()
    try:
        responseObj = requests.get(
            "{0}/rest_v2/organizations/{1}/roles".format(
                jasperUrlRoot,
                orgId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise RoleException(error.response.json()["message"])
            else:
                raise RoleException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            roleIdList.extend([role["name"] for role in json.loads(responseObj.content)["role"]])
    except Exception as error:
        raise RoleException("Unable to retreive the list of extant Jasper roles for Organization '{0}':\n{1}".format(
            orgId,
            error
        ))
    return roleIdList

def create(orgId, roleId):
    logging.getLogger("rest").debug("Creating role '{0}' for organization '{1}'.".format(
        roleId,
        orgId
    ))
    try:
        responseObj = requests.put(
            "{0}/rest_v2/organizations/{1}/roles/{2}".format(
                jasperUrlRoot,
                orgId,
                roleId
            ),
            data = "{}",
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise RoleException(error.response.json()["message"])
            else:
                raise RoleException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise RoleException("Unable to create role '{0}' for organization '{1}':\n{2}".format(
            roleId,
            orgId,
            error
        ))

def update(orgId, roleId, newRoleId):
    logging.getLogger("rest").debug("Renaming role '{0}' for organization '{1}' to '{2}'.".format(
        roleId,
        orgId,
        newRoleId
    ))
    try:
        responseObj = requests.put(
            "{0}/rest_v2/organizations/{1}/roles/{2}".format(
                jasperUrlRoot,
                orgId,
                roleId
            ),
            data = json.dumps({"name": newRoleId}),
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise RoleException(error.response.json()["message"])
            else:
                raise RoleException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise RoleException("Unable to update role '{0}' for organization '{1}' to '{2}':\n{3}".format(
            roleId,
            orgId,
            newRoleId,
            error
        ))

def delete(orgId, roleId):
    logging.getLogger("rest").debug("Deleting role '{0}' for organization '{1}'.".format(
        roleId,
        orgId
    ))
    try:
        roleDeleteResposneObj = requests.delete(
            "{0}/rest_v2/organizations/{1}/roles/{2}".format(
                jasperUrlRoot,
                orgId,
                roleId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            roleDeleteResposneObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise RoleException(error.response.json()["message"])
            else:
                raise RoleException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise RoleException("Unable to delete role '{0}' at organization '{1}':\n{2}".format(
            roleId,
            orgId,
            error
        ))

def createWorker(orgId, roleIdList=list(), roleDictList=list()): #roleDictList is for compatibility
    existingRoleIdList = get(orgId)
    
    ##compatibility section
    try:
        roleIdList.extend([roleDict["name"] for roleDict in roleDictList])
    except TypeError as error: ##workaround for bad JSON implementation FB 137516
        roleIdList.extend([roleDict.split(":")[1] for roleDict in roleDictList])
    ##
    
    for roleId in roleIdList:
        if roleId not in existingRoleIdList:
            try:
                create(orgId, roleId)
            except RoleException as error:
                logging.getLogger("rest").error(error)
                continue

def deleteWorker(orgId, roleIdList=list(), roleDictList=list()): #roleDictList is for compatibility
    
    ##compatibility section
    try:
        roleIdList.extend([roleDict["name"] for roleDict in roleDictList])
    except TypeError as error: ##workaround for bad JSON implementation FB 137516
        roleIdList.extend([roleDict.split(":")[1] for roleDict in roleDictList])
    ##
    
    for roleId in roleIdList:
        try:
            delete(orgId, roleId=roleId)
        except RoleException as error:
            logging.getLogger("rest").error(error)
            continue
