import sys
import os
import importlib
import traceback
import json
import logging
import ConfigParser
from shared import MessageMaker
from shared import pyLogger
import configure

class RestException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")

try:
    logRoot = config.get("global", "logRoot")
    logLevel = config.get("rest", "logLevel")
except ConfigParser.Error as error:
    sys.exit("Configuration error:\n{0}".format(error))

pyLogger.makeLogger(
    loggerName = "rest",
    logPath = os.path.join(logRoot, "rest.log"),
    logLevel = logLevel
)

def getOrgCredentials(orgId):
    try:
        if len(orgId.split("_")) == 2:
            client, environment = orgId.split("_")
        else:
            client = config.get("global", "client").upper()
            environment = orgId
            if not client:
                raise Exception("No client name is configured, and none could be derived from the organization ID.")
        authTuple = (
            "{0}|{1}".format(
                configure.configure(client, environment, "jasperUser"),
                orgId
            ),
            configure.configure(client, environment, "jasperPassword")
        )
    except Exception as error:
        raise RestException("Unable to derive the credentials to authenticate against the REST service with:\n{0}".format(
            error
        ))
    return authTuple

def rpcDecorator(decoratedFunction):
    functionName = decoratedFunction.__name__
    def messageHandlerWrapper(*args, **kwargs):
        try:
            replyTo = kwargs.pop("replyTo")
            correlationId = kwargs.pop("correlationId")
        except LookupError:
            decoratedFunction(*args, **kwargs)
            return
        
        logging.getLogger("rest").debug("{0} was called as an RPC function.".format(functionName))
        try:
            instance = config.get("global", "instance")
            ip = config.get("global", "ip")
            connectionTimeout = config.get("rest", "connectionTimeout")
        except ConfigParser.Error as error:
            logging.getLogger("rest").error("Configuration error:\n{0}".format(error))
            sys.exit(1)
        
        returnDict = {
            "status": "failure",
            "message": "A message has not been defined.",
            "instance": instance,
            "ip": ip
        }
        
        try:
            returnMessage = decoratedFunction(*args, **kwargs)
            returnDict["message"] = returnMessage or "The process completed succesfully."
            returnDict["status"] = "success"
        except Exception as error:
            returnDict["message"] = traceback.format_exc()
            raise
        finally:
            returnJson = json.dumps(returnDict)
            logging.getLogger("rest").debug("Publishing the following response with routing key '{0}':\n{1}".format(replyTo, returnJson))
            try:
                messageMaker = MessageMaker.MessageMaker(
                    exchangeName = "teams",
                    timeout = connectionTimeout
                )
            except MessageMaker.MessagingException as error:
                logging.getLogger("rest").error("Unable to create a MessageMaker object:\n{0}".format(error))
            try:
                messageMaker.send(
                    routingKey = replyTo,
                    message = returnJson,
                    correlationId = correlationId
                )
            except MessageMaker.MessagingException as error:
                logging.getLogger("rest").error("Unable to put a message in the exchange for the response to the request:\n{1}".format(error))
            finally:
                messageMaker.close()
    
    return messageHandlerWrapper

def messageHandler(replyTo, correlationId, message):
    logging.getLogger("rest").debug("Received message:\n{0}".format(message))
    try:
        messageDict = json.loads(message)
    except Exception as error:
        logging.getLogger("rest").error("Unable to parse message as JSON:\n{0}".format(message))
        return
    if not isinstance(messageDict, dict):
        logging.getLogger("rest").error("The JSON message is not a JSON object:\n{0}".format(message))
        return
    
    if replyTo:
        messageDict.update({"replyTo": replyTo})
    if correlationId:
        messageDict.update({"correlationId": correlationId})
    
    try:
        try:
            moduleName, functionName = messageDict.pop("action").split(".")
        except ValueError as error:
            logging.getLogger("rest").error("Unable to derive the module & function name from the 'action' parameter.")
            return
        try:
            module = importlib.import_module("jasper.{0}".format((moduleName), package=moduleName))
        except ImportError as error:
            logging.getLogger("rest").error("Unable to import the {0} module:\n{1}".format(
                moduleName,
                error
            ))
            return
        try:
            moduleFunction = getattr(module, "{0}Worker".format(functionName))
        except LookupError as error:
            logging.getLogger("rest").error("Unable to find function '{0}' in module '{1}'.".format(
                moduleName,
                "{0}Worker".format(functionName)
            ))
            return
    except Exception as error:
        logging.getLogger("rest").error("Encountered an unexpected error:\n{0}".format(traceback.format_exc()))
        return
    
    logging.getLogger("rest").debug("Calling the '{0}.{1}' function.".format(moduleName, "{0}Worker".format(functionName)))
    try:
        moduleFunction(**messageDict)
    except Exception as error:
        logging.getLogger("rest").error("{0}.{1}:\n{2}".format(
            moduleName,
            "{0}Worker".format(functionName),
            error
        ))
        logging.getLogger("rest").debug(traceback.format_exc())
        return
    logging.getLogger("rest").debug("The '{0}.{1}' function has returned.".format(moduleName, "{0}Worker".format(functionName)))
