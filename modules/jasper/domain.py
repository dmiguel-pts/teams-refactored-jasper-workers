import sys
import os
import json
import logging
import re
import errno
import shutil
import base64
import ConfigParser
import requests
import tarfile
import contextlib
import tempfile
from rest import getOrgCredentials
from rest import rpcDecorator
import user
import role
import configure

class DomainException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
except ConfigParser.Error as error:
    raise DomainException("Configuration error:\n{0}".format(error))

def createDomainsDirectory(orgId):
    authTuple = getOrgCredentials(orgId)
    try:
        responseObj = requests.get(
            "{0}/rest_v2/resources/Domains".format(jasperUrlRoot),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
            logging.getLogger("rest").debug("The 'Domains' directory of organization '{0}' already exists.".format(orgId))
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DomainException(error.response.json()["message"])
            elif error.response.status_code == 404:
                logging.getLogger("rest").debug("Creating the 'Domains' directory of organization '{0}'.".format(orgId))
                try:
                    responseObj = requests.put(
                        "{0}/rest_v2/resources/Domains".format(jasperUrlRoot),
                        params = {
                            "createFolders": False,
                            "overwrite": False
                        },
                        data = json.dumps({
                            "label": "Domains",
                            "description":"Contains domains"
                        }),
                        headers = {
                            "content-type": "application/repository.folder+json",
                            "accept": "application/json"
                        },
                        auth = authTuple
                    )
                    try:
                        responseObj.raise_for_status()
                    except requests.HTTPError as error:
                        if error.response.status_code == 400:
                            raise DomainException(error.response.json()["message"])
                except Exception as putError:
                    raise DomainException("Unable to create the 'Domains' directory of organization '{0}':\n{1}".format(
                        orgId,
                        putError
                    ))
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise DomainException(
            "Unable to verify the existence of the 'Domains' directory of organization '{0}':\n{1}".format(
                orgId,
                error
            )
        )

def getDomains(orgId):
    logging.getLogger("rest").debug("Retrieving domains for organization '{0}'.".format(orgId))
    authTuple = getOrgCredentials(orgId)
    domainIdList = list()
    
    try:
        responseObj = requests.get(
            "{0}/rest_v2/resources".format(jasperUrlRoot),
            params = {
                "folderUri": "/Domains".format(orgId),
                "type": "semanticLayerDataSource"
            },
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DomainException(error.response.json()["message"])
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            domainIdList.extend([domain["uri"].split("/")[-1:][0] for domain in json.loads(responseObj.content)["resourceLookup"]])
    except Exception as error:
        raise DomainException(
            "Unable to retreive the list of extant domains for organization '{0}':\n{1}".format(
                orgId,
                error
            )
        )
    return domainIdList

def getPermissions(orgId, domainId):
    logging.getLogger("rest").debug("Retrieving the permissions for domain '{0}' at organization '{0}'.".format(
        domainId,
        orgId
    ))
    authTuple = getOrgCredentials(orgId)
    permissionDictList = list()
    
    try:
        responseObj = requests.get(
            "{0}/rest_v2/permissions/Domains/{1}".format(
                jasperUrlRoot,
                domainId
            ),
            params = {
                "resolveAll": True,
                "effectivePermissions": True
            },
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DomainException(error.response.json()["message"])
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            for permission in json.loads(responseObj.content)["permission"]:
                if orgId in permission["recipient"]:
                    if permission["mask"] == 0:
                        read = False
                        write = False
                    elif permission["mask"] == 2:
                        read = True
                        write = False
                    elif permission["mask"] == 6:
                        read = True
                        write = True
                    else:
                        if permission["mask"] != 1:
                            logging.getLogger("rest").warn("Domain '{0}' at organization '{1}' has unexpected permission mask '{2}' for '{3}'.".format(
                                domainId,
                                orgId,
                                permission["mask"],
                                permission["recipient"]
                            ))
                        continue
                    
                    if "user" in permission["recipient"]:
                        permissionType = "user"
                    elif "role" in permission["recipient"]:
                        permissionType = "role"
                    else:
                        logging.getLogger("rest").error("Unable to parse recipient '{0}' of domain '{1}' at organization '{2}'.".format(
                            permission["recipient"],
                            domainId,
                            orgId
                        ))
                        continue
                    
                    permissionDictList.append({
                        "name": permission["recipient"].split("/")[-1:][0],
                        "type": permissionType,
                        "read": read,
                        "write": write
                    })
    
    except Exception as error:
        raise DomainException(
            "Unable to retreive the list of permissions for domain '{0}' at organization '{1}':\n{2}".format(
                domainId,
                orgId,
                error
            )
        )
    return permissionDictList

def update(orgId, domainId, dataSourceId, schemaXmlPath, securityXmlPath=None, label=None, description=None):
    authTuple = getOrgCredentials(orgId)
    logging.getLogger("rest").debug("Creating domain '{0}' at organization '{1}'.".format(
        domainId,
        orgId
    ))
    
    createDomainsDirectory(orgId)
    
    dataObj = {
        "label": label or domainId,
        "description": description or "",
        "dataSource":{
            "dataSourceReference":{
                "uri":"/public/Data_Sources/{0}".format(dataSourceId)
            }
        },
        "schema": {
            "schemaFile": {
                "label": os.path.basename(schemaXmlPath),
                "type": "xml",
                "content": base64.b64encode(open(schemaXmlPath, "rb").read())
            }
        }
    }
    if securityXmlPath:
        dataObj["securityFile"] = {
            "securityFile": {
                "label": os.path.basename(securityXmlPath),
                "type": "xml",
                "content": base64.b64encode(open(securityXmlPath, "rb").read())
            }
        }
    
    try:
        responseObj = requests.put(
            "{0}/rest_v2/resources/Domains/{1}".format(
                jasperUrlRoot,
                domainId
            ),
            params={
                "createFolders":True,
                "overwrite":True
            },
            data=json.dumps(dataObj),
            headers = {
                "accept": "application/json",
                "content-type": "application/repository.semanticLayerDataSource+json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                try:
                    raise DomainException(error.response.json()["message"])
                except (TypeError, LookupError):
                    raise DomainException(error.response.json())
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))

    except Exception as error:
        raise DomainException(
            "Unable to create domain '{0}' at organization '{1}':\n{2}".format(
                domainId,
                orgId,
                error
            )
        )
    
    try:
        responseObj = requests.post(
            "{0}/rest_v2/permissions".format(jasperUrlRoot),
            data = json.dumps({
                "uri": "/Domains/{0}".format(domainId),
                "recipient": "role:/ROLE_USER",
                "mask": "0"
            }),
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DomainException(error.response.json()["message"])
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise DomainException("Unable to remove ROLE_USER permissions from the '{0}' domain:\n{1}".format(
            domainId,
            error
        ))
    

def delete(orgId, domainId):
    logging.getLogger("rest").debug("Deleting domain '{0}' for organization '{1}'.".format(
        domainId,
        orgId
    ))
    authTuple = getOrgCredentials(orgId)
    
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/resources/Domains/{1}".format(
                jasperUrlRoot,
                domainId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DomainException(error.response.json()["message"])
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise DomainException("Unable to delete domain '{0}' at organization '{1}':\n{2}".format(
            domainId,
            orgId,
            error
        ))

def distribute(orgId, domainId, roleOrUserId, user=False, read=False, write=False):
    logging.getLogger("rest").debug("Modifying permissions of domain '{0}' for organization '{1}'.".format(
        domainId,
        orgId
    ))
    authTuple = getOrgCredentials(orgId)
    
    if write:
        permissionMask = "6"
    else:
        if read:
            permissionMask = "2"
        else:
            permissionMask = "0"
    
    if user:
        recipient = "user:/{0}/{1}".format(orgId, roleOrUserId)
    else:
        recipient = "role:/{0}/{1}".format(orgId, roleOrUserId)
    
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/permissions/Domains/{1};recipient={2}".format(
                jasperUrlRoot,
                domainId,
                re.sub("/", "%2F", recipient)
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 404:
                logging.getLogger("rest").debug("Permission '{0}' does not yet exist on the '{1}' domain, and cannot be deleted.".format(
                    roleOrUserId,
                    domainId
                ))
            elif error.response.status_code == 400:
                raise DomainException(error.response.json()["message"])
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise DomainException("Unable to delete existing '{0}' permissions on the '{1}' domain:\n{2}".format(
            roleOrUserId,
            domainId,
            error
        ))
    
    logging.getLogger("rest").debug("Setting permission '{0}' for recipient '{1}' on domain '{2}' for organization '{3}'.".format(
        permissionMask,
        recipient,
        domainId,
        orgId
    ))
    try:
        responseObj = requests.post(
            "{0}/rest_v2/permissions".format(jasperUrlRoot),
            data = json.dumps({
                "uri": "/Domains/{0}".format(domainId),
                "recipient": recipient,
                "mask": permissionMask
            }),
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DomainException(error.response.json()["message"])
            else:
                raise DomainException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise DomainException("Unable to set '{0}' permissions on the '{1}' domain:\n{2}".format(
            roleOrUserId,
            domainId,
            error
        ))


@rpcDecorator
def getWorker(orgId):
    domainIdList = getDomains(orgId)
    domainDictList = list()
    for domainId in domainIdList:
        domainDictList.append({
            "name": domainId,
            "permissions": getPermissions(orgId, domainId)
        })
    return domainDictList

@rpcDecorator
def getDomainsWorker(orgId):
    return getDomains(orgId)

@rpcDecorator
def getPermissionsWorker(orgId, domainId):
    return getPermissions(orgId, domainId)

@rpcDecorator
def updateWorker(orgId, domainId):
    logging.getLogger("rest").info("Updating domain '{0}'.".format(domainId))
    try:
        deployMount = config.get("rest", "deployMount")
    except ConfigParser.Error as error:
        raise DomainException("Configuration error:\n{0}".format(error))
    
    domainDeployDirPath = os.path.join(deployMount, "adhoc")
    packageMatches = [filename for filename in os.listdir(domainDeployDirPath) if domainId in filename]
    if len(packageMatches) != 1:
        raise DomainException("Found {0} files in '{1}' that contain '{2}'.".format(
            len(packageMatches),
            domainDeployDirPath,
            domainId
        ))
    domainPackagePath = os.path.join(domainDeployDirPath, packageMatches[0])
    extractDir = tempfile.mkdtemp(prefix = "domain")
    
    try:
        try:
            if len(orgId.split("_")) == 2:
                client, environment = orgId.split("_")
            else:
                client = config.get("global", "client").upper()
                environment = orgId
                if not client:
                    raise DomainException("No client name is configured, and it could not be derived from the organization ID.")
        except Exception as error:
            raise DomainException("Unable to derive the client and environment names for organization '{0}':\n{1}".format(orgId, error))
        dbType = configure.configure(client, environment, "dbType", "database")
        
        try:
            with contextlib.closing(tarfile.open(domainPackagePath)) as tarObj:
                try:
                    with contextlib.closing(tarObj.extractfile("domain.properties")) as propFileObj:
                        domainConfig = ConfigParser.SafeConfigParser({
                            "label": None,
                            "description": None
                        })
                        domainConfig.readfp(propFileObj)
                        domainId = domainConfig.get("domain", "id")
                        label = domainConfig.get("domain", "label")
                        description = domainConfig.get("domain", "description")
                except Exception as error:
                    raise DomainException("Unable to derive domain details from the 'domain.properties' file:\n{0}".format(
                        error
                    ))
                
                schemaXmlPath = os.path.join(extractDir, "schema.xml")
                securityXmlPath = None
                extractList = ["schema"]
                if "{0}/security.xml".format(dbType) in tarObj.getnames():
                    securityXmlPath = os.path.join(extractDir, "security.xml")
                    extractList.append("security")
                
                for name in extractList:
                    try:
                        member = tarObj.getmember("{0}/{1}.xml".format(dbType, name))
                        member.name = os.path.basename(member.name)
                        logging.getLogger("rest").debug("Extracting '{0}' from the domain package.".format(member.name))
                        tarObj.extract(member, extractDir)
                    except Exception as error:
                        raise DomainException("Unable to extract '{0}' from the domain package:\n{1}".format(
                            member.name,
                            error
                        ))
        
        except tarfile.ReadError as error:
            raise DomainException("Unable to open the domain package at '{0}':\n{1}".format(
                domainPackagePath,
                error
            ))
        
        update(
            orgId=orgId,
            domainId=domainId,
            dataSourceId="TEAMS",
            schemaXmlPath=schemaXmlPath,
            securityXmlPath=securityXmlPath,
            label=label,
            description=description
        )
    
    finally:
        try:
            shutil.rmtree(extractDir)
        except EnvironmentError as error:
            if error.errno != errno.ENOENT:
                logging.getLogger("rest").error("Unable to remove temporary directory '{0}':\n{1}".format(
                    extractDir,
                    error
                ))

def deleteWorker(orgId, domainId):
    logging.getLogger("rest").info("Deleting domain '{0}'.".format(domainId))
    delete(orgId, domainId)

def distributeWorker(orgId, domainId, roleDictList=list(), userDictList=list()):
    logging.getLogger("rest").info("Distributing domain '{0}'.".format(domainId))
    roleOrUserDictList = list()
    existingUsernameList = [userDict["username"] for userDict in user.get(orgId)]
    existingRoleIdList = role.get(orgId)
    
    for userDict in userDictList:
        userDict["user"] = True
        roleOrUserDictList.append(userDict)
        if userDict["name"] not in existingUsernameList:
            try:
                user.update(orgId, userDict["name"], external=True)
            except user.UserException as error:
                logging.getLogger("rest").error(error)
                continue
    
    for roleDict in roleDictList:
        roleDict["user"] = False
        roleOrUserDictList.append(roleDict)
        if roleDict["name"] not in existingRoleIdList:
            try:
                role.create(orgId, roleDict["name"])
            except role.RoleException as error:
                logging.getLogger("rest").error(error)
                continue
    
    for roleOrUserDict in roleOrUserDictList:
        try:
            distribute(
                orgId=orgId,
                domainId=domainId,
                roleOrUserId=roleOrUserDict["name"],
                user=roleOrUserDict["user"],
                read=roleOrUserDict["read"],
                write=roleOrUserDict["write"]
            )
        except DomainException as error:
            logging.getLogger("rest").error(error)
            continue
