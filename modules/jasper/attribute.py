import sys
import os
import json
import logging
import ConfigParser
import requests

class AttributeException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
    authTuple = (
        config.get("rest", "jasperUser"),
        config.get("rest", "jasperPassword")
    )
except ConfigParser.Error as error:
    raise AttributeException("Configuration error:\n{0}".format(error))

def get(orgId, username, attributeNameList=list()):
    logging.getLogger("rest").debug("Retrieving attributes for user '{0}' at orginization '{1}'.".format(
        username,
        orgId
    ))
    attributeDictList = list()
    try:
        responseObj = requests.get(
            "{0}/rest_v2/organizations/{1}/users/{2}/attributes".format(
                jasperUrlRoot,
                orgId,
                username
            ),
            headers = {
                "accept": "application/json"
            },
            params = {
                "name": attributeNameList
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise AttributeException(error.response.json()["message"])
            else:
                raise AttributeException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            attributeDictList.extend([attribute for attribute in json.loads(responseObj.content)["attribute"]])
    except Exception as error:
        raise AttributeException("Unable to derive the attributes of user '{0}' in organization '{1}':\n{2}".format(
            username,
            orgId,
            error
        ))
    return attributeDictList

def update(orgId, username, attributeName, attributeValue):
    logging.getLogger("rest").debug("Updating the '{0}' attribute for the '{1}' user at organization '{2}' to '{3}'.".format(
        attributeName,
        username,
        orgId,
        attributeValue
    ))
    try:
        responseObj = requests.put(
            "{0}/rest_v2/organizations/{1}/users/{2}/attributes/{3}".format(
                jasperUrlRoot,
                orgId,
                username,
                attributeName
            ),
            data = json.dumps({
                "name": attributeName,
                "value": attributeValue
            }),
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise AttributeException(error.response.json()["message"])
            else:
                raise AttributeException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise AttributeException("Unable to update the '{0}' attribute for the '{1}' user at organization '{2}' to '{3}':\n{2}".format(
            attributeName,
            username,
            orgId,
            attributeValue,
            error
        ))

def delete(orgId, username, attributeName):
    logging.getLogger("rest").debug("Deleting the '{0}' attribute from the '{1}' user at organization '{2}'.".format(
        attributeName,
        username,
        orgId
    ))
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/organizations/{1}/users/{2}/attributes".format(
                jasperUrlRoot,
                orgId,
                username
            ),
            params = {
                "name": attributeName
            },
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise AttributeException(error.response.json()["message"])
            else:
                raise AttributeException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise AttributeException("Unable to delete the '{0}' attribute for username '{1}' at organization '{2}' :\n{3}".format(
            attributeName,
            username,
            orgId,
            error
        ))

def updateWorker(orgId, username, attributeName, attributeValue):
    update(orgId, username, attributeName, attributeValue)

def deleteWorker(orgId, username, attributeNameList):
    for attributeName in attributeNameList:
        try:
            delete(orgId, username, attributeName)
        except AttributeException as error:
            logging.getLogger("rest").error(error)
            continue
