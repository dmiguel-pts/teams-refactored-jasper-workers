import sys
import os
import json
import logging
import ConfigParser
import re
import itertools
import requests
import configure
import role
import user
import dataSource
import domain

class OrganizationException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
    authTuple = (
        config.get("rest", "jasperUser"),
        config.get("rest", "jasperPassword")
    )
except ConfigParser.Error as error:
    raise OrganizationException("Configuration error:\n{0}".format(error))

def get():
    logging.getLogger("rest").debug("Retrieving the list of organizations.")
    orgIdList = list()
    try:
        responseObj = requests.get(
            "{0}/rest_v2/organizations".format(jasperUrlRoot),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise OrganizationException(error.response.json()["message"])
            else:
                raise OrganizationException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            orgIdList.extend([org["id"] for org in json.loads(responseObj.content)["organization"]])
    except Exception as error:
        raise OrganizationException("Unable to derive the list of organizations:\n{0}".format(error))
    return orgIdList

def create(orgId, orgAlias=None, orgDescription=None):
    logging.getLogger("rest").debug("Creating organization '{0}'".format(
        orgId
    ))
    orgCreateDict = {
        "id": orgId,
        "tenantName": orgId,
        "alias": orgAlias or orgId
    }
    if orgDescription:
        orgCreateDict["tenantDesc"] = orgDescription
    
    try:
        responseObj = requests.post(
            "{0}/rest_v2/organizations".format(jasperUrlRoot),
            params = {"createDefaultUsers": False},
            data = json.dumps(orgCreateDict),
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise OrganizationException(error.response.json()["message"])
            else:
                raise OrganizationException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise OrganizationException("Unable to create a new organization with an ID and name of '{0}', and an alias of '{1}':\n{2}".format(
            orgId,
            orgAlias,
            error
        ))

def update(orgId, orgAlias=None, orgDescription=None):
    logging.getLogger("rest").debug("Updating organization '{0}'.".format(
        orgId
    ))
    
    orgUpdateDict = {"id": orgId}
    if orgAlias:
        orgUpdateDict["alias"] = orgAlias
    if orgDescription:
        orgUpdateDict["tenantDesc"] = orgDescription
    
    try:
        responseObj = requests.put(
            "{0}/rest_v2/organizations/{1}/".format(
                jasperUrlRoot,
                orgId
            ),
            data = json.dumps(orgUpdateDict),
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise OrganizationException(error.response.json()["message"])
            else:
                raise OrganizationException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise OrganizationException("Unable to update organization '{0}':\n{1}".format(
            orgId,
            error
        ))

def delete(orgId):
    logging.getLogger("rest").debug("Deleting organization '{0}'.".format(
        orgId
    ))
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/organizations/{1}".format(
                jasperUrlRoot,
                orgId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise OrganizationException(error.response.json()["message"])
            else:
                raise OrganizationException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise OrganizationException("Unable to delete Organization '{0}':\n{1}".format(
            orgId,
            error
        ))

def initializeWorker(orgId, orgAlias=None, jasperadminPw="", roleIdList=list(), roleDictList=list()): #roleDictList is for compatibility
    logging.getLogger("rest").info("Initializing organization '{0}'.".format(orgId))
    
    orgIdList = get()
    if orgId in orgIdList:
        update(orgId, orgAlias)
    else:
        create(orgId, orgAlias)
    
    user.update(
        orgId=orgId,
        username="jasperadmin",
        fullName="jasperadmin",
        password=jasperadminPw,
        roleDictList=[
            {"roleId": "ROLE_ADMINISTRATOR", "orgId": None},
            {"roleId": "ROLE_USER", "orgId": None}
        ]
    )
    
    user.createUsersDirectory(orgId)
    domain.createDomainsDirectory(orgId)
    
    logging.getLogger("rest").debug("Synchronizing roles at organization '{0}'.".format(orgId))
    existingRoleIdList = role.get(orgId)
    
    ##compatibility section
    try:
        roleIdList.extend([roleDict["name"] for roleDict in roleDictList])
    except TypeError as error: ##workaround for bad JSON implementation FB 137516
        roleIdList.extend([roleDict.split(":")[1] for roleDict in roleDictList])
    ##
    
    for existingNonMatchingRoleId in [roleId for roleId in existingRoleIdList if roleId not in roleIdList]:
        try:
            role.delete(
                orgId=orgId,
                roleId=existingNonMatchingRoleId
            )
        except role.RoleException as error:
            logging.getLogger("rest").error(error)
            continue
    
    for roleId in roleIdList:
        if roleId not in existingRoleIdList:
            try:
                role.create(
                    orgId=orgId,
                    roleId=roleId
                )
            except role.RoleException as error:
                logging.getLogger("rest").error(error)
                continue
    
    try:
        if len(orgId.split("_")) == 2:
            client, environment = orgId.split("_")
        else:
            client = config.get("global", "client").upper()
            environment = orgId
            if not client:
                raise OrganizationException("No client name is configured, and it could not be derived from the organization ID.")
    except Exception as error:
        raise OrganizationException("Unable to derive the client and environment names for organization '{0}':\n{1}".format(orgId, error))
    
    dbType = configure.configure(
        client=client,
        environment=environment,
        option="dbtype",
        instanceType="database"
    )
    jdbcUrl = configure.configure(
        client=client,
        environment=environment,
        option="url",
        instanceType="database"
    )
    
    if dbType == "db2":
        dbRegexGroups = re.search("//(.*)/(.*):", jdbcUrl).groups()
    elif dbType == "mssql":
        dbRegexGroups = re.search("//(.*?);databaseName=(.*?);", jdbcUrl).groups()
    elif dbType == "oracle":
        dbRegexGroups = re.search("@(.*):(.*)", jdbcUrl).groups()
    dbIp, dbPort, dbName = list(itertools.chain(*[result.split(":") for result in dbRegexGroups]))
    
    dbUsername = configure.configure(
        client=client,
        environment=environment,
        option="username",
        instanceType="database"
    )
    dbPassword = configure.configure(
        client=client,
        environment=environment,
        option="password",
        instanceType="database"
    )
    
    dataSource.update(
        orgId=orgId,
        dataSourceId="TEAMS",
        dbType=dbType,
        ip=dbIp,
        port=dbPort,
        databaseName=dbName,
        username=dbUsername,
        password=dbPassword
    )
    
    logging.getLogger("rest").debug("Organization '{0}' has been initialized.".format(orgId))

def createWorker(orgId, orgAlias=None):
    orgIdList = get()
    if orgId not in orgIdList:
        create(orgId, orgAlias)

def deleteWorker(orgId):
    delete(orgId)
