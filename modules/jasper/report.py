import sys
import os
import json
import logging
import re
import ConfigParser
import requests
from rest import getOrgCredentials

class ReportException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("~/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
except ConfigParser.Error as error:
    raise ReportException("Configuration error:\n{0}".format(error))

def get(orgId):
    logging.getLogger("rest").debug("Retrieving reports for organization '{0}'.".format(orgId))
    authTuple = getOrgCredentials(orgId)
    reportIdList = list()
    
    try:
        responseObj = requests.get(
            "{0}/rest_v2/resources".format(jasperUrlRoot),
            params = {
                "folderUri": "Reports",
                "type": "reportUnit"
            },
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise ReportException(error.response.json()["message"])
            else:
                raise ReportException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            reportIdList.extend([report["uri"].split("/")[-1:] for report in json.loads(responseObj.content)["resourceLookup"]])
    except Exception as error:
        raise ReportException(
            "Unable to retreive the list of extant reports for organization '{0}':\n{1}".format(
                orgId,
                error
            )
        )
    return reportIdList

def update(orgId, reportId, dataSourceId, jrxmlPath, inputControlIdList=list(), resourceDictList=list(), label=None, description=None):
    logging.getLogger("rest").debug("Creating report '{0}' at organization '{1}'.".format(
        reportId,
        orgId
    ))
    authTuple = getOrgCredentials(orgId)
    
    try:
        inputControlList = list()
        for inputControlId in inputControlIdList:
            inputControlList.append({
                "inputControlReference": {
                    "uri": "/organizations/{0}/Input_Controls/{1}".format(
                        orgId,
                        inputControlId
                    )
                }
            })
    except Exception as error:
        raise ReportException("Unable to parse inputControlIdList:\n{0}".format(error))
    
    try:
        resourceList = list()
        for resourceDict in resourceDictList:
            
            #so, the dict tells us if the included file is getting uploaded baked in or not.
            #if it is, we just do so
            #otherwise we have to upload/overwrite an existing resource in addition to providing the url.
            
            if resourceDict["url"]:
                resourceList.append({
                    "name": resourceDict["name"],
                    "fileRefernce": {
                        "uri": resourceDict["url"]
                    }
                })
            else:
                resourceList.append({
                    "name": resourceDict["name"],
                    "file": {
                        "fileResource": {
                            "label": resourceDict["name"],
                            "type": resourceDict["type"],
                            "content": base64.b64encode(
                                open(resourceDict["path"], "rb").read()
                            )
                        }
                    }
                })
    except Exception as error:
        raise ReportException("Unable to parse resourceDictList:\n{0}".format(error))
    
    try:
        responseObj = requests.put(
            "{0}/rest_v2/resources/Reports".format(
                jasperUrlRoot
            ),
            params={
                "createFolders":True,
                "overwrite":True
            },
            data=json.dumps({
                "label": label or reportId,
                "description": description or "",
                "dataSource":{
                    "dataSourceReference":{
                        "uri":"/public/Data_Sources/{0}".format(dataSourceId)
                    }
                },
                "jrxml": {
                    "jrxmlFile": {
                        "label": os.path.basename(jrxmlPath),
                        "type": "xml",
                        "content": base64.b64encode(open(jrxmlPath, "rb").read())
                    }
                },
                "inputControls": inputControlList,
                "resources": resourceList
            }),
            headers = {
                "accept": "application/json",
                "content-type": "application/repository.semanticLayerDataSource+json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise ReportException(error.response.json()["message"])
            else:
                raise ReportException("HTTP status code '{0}'".format(error.response.status_code))

    except Exception as error:
        raise ReportException(
            "Unable to create report '{0}' at organization '{1}':\n{2}".format(
                reportId,
                orgId,
                error
            )
        )

def delete(orgId, reportId):
    logging.getLogger("rest").debug("Deleting report '{0}' for organization '{1}'.".format(
        reportId,
        orgId
    ))
    authTuple = getOrgCredentials(orgId)
    
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/resources/Reports/{1}".format(
                jasperUrlRoot,
                reportId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise ReportException(error.response.json()["message"])
            else:
                raise ReportException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise ReportException("Unable to delete report '{0}' at organization '{1}':\n{2}".format(
            reportId,
            orgId,
            error
        ))

