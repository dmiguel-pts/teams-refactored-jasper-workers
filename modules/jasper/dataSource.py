import sys
import os
import json
import logging
import re
import ConfigParser
import requests

class DataSourceException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
    authTuple = (
        config.get("rest", "jasperUser"),
        config.get("rest", "jasperPassword")
    )
except ConfigParser.Error as error:
    raise DataSourceException("Configuration error:\n{0}".format(error))

def get(orgId):
    logging.getLogger("rest").debug("Retrieving data sources for organization '{0}'.".format(orgId))
    dataSourceIdList = list()
    try:
        responseObj = requests.get(
            "{0}/rest_v2/resources".format(jasperUrlRoot),
            params = {
                "folderUri": "/organizations/{0}/Data_Sources".format(orgId),
                "type": "jdbcDataSource"
            },
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DataSourceException(error.response.json()["message"])
            else:
                raise DataSourceException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            dataSourceIdList.extend([dataSource["uri"].split("/")[-1:] for dataSource in json.loads(responseObj.content)["resourceLookup"]])
    except Exception as error:
        raise DataSourceException(
            "Unable to retreive the list of extant JDBC data sources for organization '{0}':\n{1}".format(
                orgId,
                error
            )
        )
    return dataSourceIdList

def update(orgId, dataSourceId, dbType, ip, port, databaseName, username, password):
    logging.getLogger("rest").debug("Creating datbase attributes for organization '{0}' and public '{1}' data source.".format(
        orgId,
        dataSourceId
    ))
    
    if dbType == "db2":
        driverClass = "tibcosoftware.jdbc.db2.DB2Driver"
        jdbcProtocol = "jdbc:tibcosoftware:db2"
    elif dbType == "mssql":
        driverClass = "tibcosoftware.jdbc.sqlserver.SQLServerDriver"
        jdbcProtocol = "jdbc:tibcosoftware:sqlserver"
    elif dbType == "oracle":
        driverClass = "tibcosoftware.jdbc.oracle.OracleDriver"
        jdbcProtocol = "jdbc:tibcosoftware:oracle"
    elif dbType == "pg":
        driverClass = "org.postgresql.Driver"
        jdbcProtocol = "jdbc:postgresql"
    else:
        raise DataSourceException("Received unknown 'dbType' argument: {0}".format(dbType))
    jdbcUrl = "{0}://{1}:{2};databaseName={3}".format(
        jdbcProtocol,
        ip,
        port,
        databaseName
    )
    
    attributeDict = {
        "dbDriver": driverClass,
        "dbUrl": jdbcUrl,
        "dbUser": username,
        "dbPassword": password
    }
    for attribute in attributeDict:
        try:
            responseObj = requests.put(
                "{0}/rest_v2/organizations/{1}/attributes/{2}".format(
                    jasperUrlRoot,
                    orgId,
                    attribute
                ),
                data = json.dumps({
                    "name": attribute,
                    "value": attributeDict[attribute]
                }),
                headers = {
                    "content-type": "application/json",
                    "accept": "application/json"
                },
                auth = authTuple
            )
            try:
                responseObj.raise_for_status()
            except requests.HTTPError as error:
                if error.response.status_code == 400:
                    raise DataSourceException(error.response.json()["message"])
                else:
                    raise DataSourceException("HTTP status code '{0}'".format(error.response.status_code))
        except Exception as error:
            raise DataSourceException("Unable to create attribute '{0}' for organization '{1}':\n{2}".format(
                attribute,
                orgId,
                error
            ))
    
    try:
        responseObj = requests.get(
            "{0}/rest_v2/resources/public/Data_Sources/{1}".format(
                jasperUrlRoot,
                dataSourceId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
            logging.getLogger("rest").debug("The public '{0}' data source already exists.".format(dataSourceId)) ##update it in some way if this is the case
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DataSourceException(error.response.json()["message"])
            elif error.response.status_code == 404:
                logging.getLogger("rest").debug("Creating the existing public '{0}' data source.".format(dataSourceId))
                try:
                    responseObj = requests.put(
                        "{0}/rest_v2/resources/public/Data_Sources/{1}".format(
                            jasperUrlRoot,
                            dataSourceId
                        ),
                        data = json.dumps({
                            "label": dataSourceId,
                            "driverClass": "{attribute('dbDriver','Tenant')}",
                            "connectionUrl": "{attribute('dbUrl','Tenant')}",
                            "username": "{attribute('dbUser','Tenant')}",
                            "password": "{attribute('dbPassword','Tenant')}"
                        }),
                        headers = {
                            "content-type": "application/repository.jdbcDataSource+json"
                        },
                        auth = authTuple
                    )
                    try:
                        responseObj.raise_for_status()
                    except requests.HTTPError as error:
                        if error.response.status_code == 400:
                            raise DataSourceException(error.response.json()["message"])
                        else:
                            raise DataSourceException("HTTP status code '{0}'".format(error.response.status_code))
                except Exception as error:
                    raise DataSourceException("Unable to create public '{0}' data source:\n{1}".format(dataSourceId, error))
            else:
                raise DataSourceException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise DataSourceException("Unable to verify the existence of the public '{0}' data source:\n{1}".format(
            dataSourceId,
            error
        ))
    
    logging.getLogger("rest").debug("Setting the permissions on the public '{0}' data source.".format(dataSourceId))
    try:
        responseObj = requests.put(
            "{0}/rest_v2/permissions/public/Data_Sources/{1}".format(
                jasperUrlRoot,
                dataSourceId
            ),
            data = json.dumps({
                "permission": [
                    {
                        "recipient": "role:/ROLE_USER",
                        "mask": "32"
                    }
                ]
            }),
            headers = {
                "content-type": "application/collection+json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DataSourceException(error.response.json()["message"])
            else:
                raise DataSourceException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise DataSourceException("Unable to set permissions on the public '{0}' data source:\n{1}".format(
            dataSourceId,
            error
        ))

def delete(orgId, dataSourceId):
    logging.getLogger("rest").debug("Deleting data source '{0}' for organization '{1}'.".format(
        dataSourceId,
        orgId
    ))
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/resources/organizations/{1}/Data_Sources/{2}".format(
                jasperUrlRoot,
                orgId,
                dataSourceId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise DataSourceException(error.response.json()["message"])
            else:
                raise DataSourceException("HTTP status code '{0}'".format(error.response.status_code))

    except Exception as error:
        raise DataSourceException("Unable to delete data source '{0}' at organization '{1}':\n{2}".format(
            dataSourceId,
            orgId,
            error
        ))
