import sys
import os
import json
import logging
import ConfigParser
import requests
from jasper import role
from jasper import attribute

class UserException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")
try:
    jasperUrlRoot = config.get("rest", "jasperUrlRoot")
    authTuple = (
        config.get("rest", "jasperUser"),
        config.get("rest", "jasperPassword")
    )
except ConfigParser.Error as error:
    raise UserException("Configuration error:\n{0}".format(error))

def createUsersDirectory(orgId, username=None):
    try:
        responseObj = requests.get(
            "{0}/rest_v2/resources/organizations/{1}/Users".format(
                jasperUrlRoot,
                orgId
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
            logging.getLogger("rest").debug("The 'Users' directory of organization '{0}' already exists.".format(orgId))
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise UserException(error.response.json()["message"])
            elif error.response.status_code == 404:
                logging.getLogger("rest").debug("Creating the 'Users' directory of organization '{0}'.".format(orgId))
                try:
                    responseObj = requests.put(
                        "{0}/rest_v2/resources/organizations/{1}/Users".format(
                            jasperUrlRoot,
                            orgId
                        ),
                        params = {
                            "createFolders": False,
                            "overwrite": False
                        },
                        data = json.dumps({
                            "label": "Users",
                            "description":"Contains user-specific directories"
                        }),
                        headers = {
                            "content-type": "application/repository.folder+json",
                            "accept": "application/json"
                        },
                        auth = authTuple
                    )
                    try:
                        responseObj.raise_for_status()
                    except requests.HTTPError as error:
                        if error.response.status_code == 400:
                            raise UserException(error.response.json()["message"])
                except Exception as putError:
                    raise UserException("Unable to create the 'Users' directory of organization '{0}':\n{1}".format(
                        orgId,
                        putError
                    ))
            else:
                raise UserException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise UserException(
            "Unable to verify the existence of the 'Users' directory of organization '{0}':\n{1}".format(
                orgId,
                error
            )
        )

def get(orgId, searchString=None, roleList=list(), roleListOr=False):
    logging.getLogger("rest").debug("Retrieving users from organization '{0}'.".format(orgId))
    userList = list()
    paramsDict = dict()
    if searchString:
        paramsDict["search"] = searchString
    if roleList:
        paramsDict["requiredRole"] = roleList
    if roleListOr:
        paramsDict["hasAllRequiredRoles"] = False
    try:
        responseObj = requests.get(
            "{0}/rest_v2/organizations/{1}/users".format(
                jasperUrlRoot,
                orgId
            ),
            params = paramsDict,
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise UserException(error.response.json()["message"])
            else:
                raise UserException("HTTP status code '{0}'".format(error.response.status_code))
        if responseObj.content:
            userList.extend(
                [{"username": user["username"], "fullName": user["fullName"]} for user in json.loads(responseObj.content)["user"]]
            )
    except Exception as error:
        raise UserException("Unable to derive the list of users in organization '{0}':\n{1}".format(
            orgId,
            error
        ))
    return userList

def update(orgId, username, enabled=True, fullName=None, password=None, emailAddress=None, external=False, roleDictList=list()):
    logging.getLogger("rest").debug("Updating username '{0}' at organization '{1}'.".format(
        username,
        orgId
    ))
    userDataDict = dict()
    userDataDict["enabled"] = enabled
    userDataDict["roles"] = [{"name": "ROLE_USER"}]
    if fullName:
        userDataDict["fullName"] = fullName
    if password:
        userDataDict["password"] = password
    if emailAddress:
        userDataDict["emailAddress"] = emailAddress
    if external:
        userDataDict["externallyDefined"] = external
    if roleDictList:
        userDataDict["roles"].extend([{
            "name": roleDict["roleId"],
            "tenantId": roleDict["orgId"]
        } for roleDict in roleDictList])
    
    adminUserDictList = get(
        orgId,
        searchString=None,
        roleList=["ROLE_ADMINISTRATOR"],
        roleListOr=False
    )
    if username in [adminUserDict["username"] for adminUserDict in adminUserDictList]:
        userDataDict["roles"].append({
            "name": "ROLE_ADMINISTRATOR"
        })
    
    logging.getLogger("rest").debug(
        "User details:\n{0}".format(
            "\n".join(["{0}: {1}".format(item, userDataDict[item]) for item in userDataDict])
        )
    )
    try:
        responseObj = requests.put(
            "{0}/rest_v2/organizations/{1}/users/{2}".format(
                jasperUrlRoot,
                orgId,
                username
            ),
            data = json.dumps(userDataDict),
            headers = {
                "content-type": "application/json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise UserException(error.response.json()["message"])
    except Exception as error:
        raise UserException("Unable to update username '{0}' at organization '{1}':\n{2}\n{3}".format(
            username,
            orgId,
            error,
            userDataDict
        ))
    
    createUsersDirectory(orgId, username)
    
    try:
        responseObj = requests.get(
            "{0}/rest_v2/resources/organizations/{1}/Users/{2}".format(
                jasperUrlRoot,
                orgId,
                username
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
            logging.getLogger("rest").debug("The user-specific directory for '{0}' at organization '{1}' already exists.".format(
                username,
                orgId
            ))
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise UserException(error.response.json()["message"])
            elif error.response.status_code == 404:
                try:
                    responseObj = requests.put(
                        "{0}/rest_v2/resources/organizations/{1}/Users/{2}".format(
                            jasperUrlRoot,
                            orgId,
                            username
                        ),
                        params = {
                            "createFolders": False,
                            "overwrite": False
                        },
                        data = json.dumps({
                            "label": fullName or username,
                            "description":"Private directory for {0}".format(fullName or username)
                        }),
                        headers = {
                            "content-type": "application/repository.folder+json",
                            "accept": "application/json"
                        },
                        auth = authTuple
                    )
                    try:
                        responseObj.raise_for_status()
                    except requests.HTTPError as error:
                        if error.response.status_code == 400:
                            raise UserException(error.response.json()["message"])
                        else:
                            raise UserException("HTTP status code '{0}'".format(error.response.status_code))
                except Exception as error:
                    raise UserException(
                        "Unable to create the user-specific directory for '{0}' at organization '{1}':\n{2}".format(
                            username,
                            orgId,
                            error
                        )
                    )
            else:
                raise UserException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise UserException("Unable to ensure the existence of the user-specific directory for '{0}' at organization '{1}':\n{2}".format(
            username,
            orgId,
            error
        ))
    
    logging.getLogger("rest").debug("Setting the permissions on the user-specific directory for '{0}' at organization '{1}'.".format(
        username,
        orgId
    ))
    try:
        responseObj = requests.put(
            "{0}/rest_v2/permissions/organizations/{1}/Users/{2}".format(
                jasperUrlRoot,
                orgId,
                username
            ),
            data = json.dumps({
                "permission": [
                    {
                        "recipient": "role:/ROLE_USER",
                        "mask": "0"
                    },
                    {
                        "recipient": "user:/{0}/{1}".format(
                            orgId,
                            username
                        ),
                        "mask": "30"
                    }
                ]
            }),
            headers = {
                "content-type": "application/collection+json",
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise UserException(error.response.json()["message"])
            else:
                raise UserException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise UserException("Unable to set permissions on the user-specific directory for '{0}' at organization '{1}':\n{2}".format(
            username,
            orgId,
            error
        ))

def delete(orgId, username):
    logging.getLogger("rest").debug("Deleting username '{0}' at organization '{1}'.".format(
        username,
        orgId
    ))
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/organizations/{1}/users/{2}".format(
                jasperUrlRoot,
                orgId,
                username
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise UserException(error.response.json()["message"])
            else:
                raise UserException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise UserException("Unable to delete username '{0}' at organization '{1}':\n{2}".format(
            username,
            orgId,
            error
        ))
    
    try:
        responseObj = requests.delete(
            "{0}/rest_v2/resources/organizations/{1}/Users/{2}".format(
                jasperUrlRoot,
                orgId,
                username
            ),
            headers = {
                "accept": "application/json"
            },
            auth = authTuple
        )
        try:
            responseObj.raise_for_status()
        except requests.HTTPError as error:
            if error.response.status_code == 400:
                raise UserException(error.response.json()["message"])
            else:
                raise UserException("HTTP status code '{0}'".format(error.response.status_code))
    except Exception as error:
        raise UserException(
            "Unable to delete the user-specific directory for '{0}' at organization '{1}':\n{2}".format(
                username,
                orgId,
                error
            )
        )

def updateWorker(orgId, username, enabled=True, fullName=None, perId=None, password=None, emailAddress=None, external=True, roleIdList=list(), attributeDictList=list(), roleDictList=list()): #roleDictList is for compatibility
    logging.getLogger("rest").info("Updating user '{0}'".format(username))
    existingRoleIdList = role.get(orgId)
    
    ##compatibility section
    try:
        roleIdList.extend([roleDict["name"] for roleDict in roleDictList])
    except TypeError as error: ##workaround for bad JSON implementation FB 137516
        roleIdList.extend([roleDict.split(":")[1] for roleDict in roleDictList])
    ##
    
    for roleId in roleIdList:
        if roleId not in existingRoleIdList:
            try:
                role.create(orgId, roleId)
            except role.RoleException as error:
                logging.getLogger("rest").error(error)
                continue
    
    if fullName == "null":
        fullName = None
    if perId == "null":
        perId = None
    if password == "null":
        password = None
    if emailAddress == "null":
        email = None
    if fullName and perId:
        fullName = "{0} ({1})".format(
            fullName,
            perId
        )
    
    update(
        orgId=orgId,
        username=username,
        fullName=fullName,
        password=password,
        emailAddress=emailAddress,
        external=external,
        roleDictList=[{
            "roleId": roleId,
            "orgId": orgId
        } for roleId in roleIdList]
    )
    
    existingAttributeDictList = attribute.get(orgId, username)
    for attributeDict in attributeDictList:
        if attributeDict not in existingAttributeDictList:
            if attributeDict["name"] and attributeDict["value"]:
                try:
                    attribute.update(
                        orgId=orgId,
                        username=username,
                        attributeName=attributeDict["name"],
                        attributeValue=attributeDict["value"]
                    )
                except attribute.AttributeException as error:
                    logging.getLogger("rest").error(error)
                    continue

def deleteWorker(orgId, username):
    delete(orgId, username)
