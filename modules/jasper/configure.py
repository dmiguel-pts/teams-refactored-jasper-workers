import sys
import os
import shutil
import traceback
import logging
import json
import argparse
import ConfigParser
from shared import pyLogger
from shared import MessageMaker
from jasper import register

class ConfigurationException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")

try:
    logRoot = config.get("global", "logRoot")
    logLevel = config.get("configure", "logLevel")
except ConfigParser.Error as error:
    raise ConfigurationException("Configuration error:\n{0}".format(error))

pyLogger.makeLogger(
    loggerName = "configure",
    logPath = os.path.join(logRoot, "configure.log"),
    logLevel = logLevel
)

parserHelp = """Set or display environment-wide configuration on the CLI."""

def makeParser(parser):
    if not isinstance(parser, argparse.ArgumentParser):
        sys.exit("Please pass this method a valid instance of argparse.ArgumentParser")
    
    try:
        client = config.get("global", "client")
    except ConfigParser.NoOptionError as error:
        parser.add_argument(
            "-c",
            "--client",
            required = True,
            help = "The client to set or display the configuration for (e.g. CARR).  This is required if there is no default in the configuration file."
        )
    except ConfigParser.NoSectionError as error:
        sys.exit("Configuration error:\n{0}".format(error))
    else:
        parser.add_argument(
            "-c",
            "--client",
            default = client,
            help = "The client to set or display the configuration for (default: %(default)s).  This is used to override the default in the configuration file (i.e. for multi-client environments)."
        )
    parser.add_argument(
        "environment",
        help = "The environment to set or display the configuration for."
    )
    parser.add_argument(
        "option",
        help = "The configuration option to set or display (e.g. dbType)."
    )
    parser.add_argument(
        "value",
        nargs = "?",
        default = None,
        help = "The value to set the option to."
    )
    
    parser.add_argument(
        "-t",
        "--type",
        dest = "instanceType",
        default = "jasper",
        help = "The type of server instance to display the configuration of (default: %(default)s)."
    )
    
    parser.set_defaults(func=parserWrapper)

def parserWrapper(func, moduleName, client, environment, option, value, instanceType):
    try:
        logging.getLogger("configure").info(configure(client, environment, option, instanceType, value))
    except ConfigurationException as error:
        logging.getLogger("configure").error(error)
    except Exception as error:
        logging.getLogger("configure").error("An unexpected error occured:\n{0}".format(traceback.format_exc()))

def configure(client, environment, option, instanceType="jasper", value=None):
    if not option:
        raise ConfigurationException("A null argument was provided for a required parameter.")
    
    try:
        register.validateRegistry()
    except register.RegistrationException as error:
        raise ConfigurationException(error.message)
    
    try:
        connectionTimeout = config.get("configure", "connectionTimeout")
    except ConfigParser.Error as error:
        raise ConfigurationException("Configuration error:\n{0}".format(error))
    
    if value:
        logging.getLogger("configure").info("Setting the value of the '{0}' option for the {1}-{2} environment to '{3}'.".format(
            option,
            client.upper(),
            environment.upper(),
            value
        ))
    else:
        logging.getLogger("configure").info("Retrieving the value of the '{0}' option for the {1}-{2} environment.".format(
            option,
            client.upper(),
            environment.upper()
        ))
    
    message = json.dumps({
        "client": client,
        "environment": environment,
        "instanceType": instanceType,
        "option": option,
        "value": value
    })
    
    logging.getLogger("configure").debug("Sending the following message with a routing key of 'cli.configure': {0}".format(message))

    try:
        messageMaker = MessageMaker.MessageMaker(
            exchangeName = "teams",
            timeout = connectionTimeout
        )
    except MessageMaker.MessagingException as error:
        raise ConfigurationException("Unable to create a MessageMaker object:\n{0}".format(error))
    try:
        response = messageMaker.send(
            routingKey = "cli.configure",
            message = message,
            rpc = True,
            expectedResponses = 1
        )
    except MessageMaker.MessagingException as error:
        raise ConfigurationException("Unable to contact the CLI server:\n{0}".format(error))
    finally:
        messageMaker.close()
    
    try:
        responseDict = json.loads(response)
    except Exception as error:
        raise ConfigurationException("Unable to parse the JSON message:\n{0}\n{1}".format(
            response,
            error
        ))
    try:
        message = responseDict["message"]
        status = responseDict["status"]
        responseDetails = responseDict["details"] 
    except KeyError as error:
        raise ConfigurationException("Unable to parse response from the CLI:\n{0}\n{1}".format(
            responseDict,
            error
        ))

    if status == "failure":
        if value:
            raise ConfigurationException("Unable to set the value of the '{0}' option in the '{1}' configuration on the CLI to '{2}':\n{3}".format(
                option,
                instanceType,
                value,
                message
            ))
        else:
            raise ConfigurationException("Unable retreive the value of the '{0}' option in the '{1}' configuration on the CLI:\n{2}".format(
                option,
                instanceType,
                message
            ))
    if len(responseDetails) != 1:
        raise ConfigurationException("Unable to parse the response from the CLI:\n{0}".format(responseDetails))
    return responseDetails[0]["message"]
