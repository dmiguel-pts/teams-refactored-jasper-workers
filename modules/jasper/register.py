import sys
import os
import pwd
import socket
import errno
import traceback
import logging
import json
import argparse
import ConfigParser
from shared import pyLogger
from shared import execute
from shared import MessageMaker
import configure

class RegistrationException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")

try:
    logRoot = config.get("global", "logRoot")
    logLevel = config.get("mount", "logLevel")
except ConfigParser.Error as error:
    raise RegistrationException("Configuration error:\n{0}".format(error))

pyLogger.makeLogger(
    loggerName = "register",
    logPath = os.path.join(logRoot, "register.log"),
    logLevel = logLevel
)

parserHelp = """Register this server with the CLI."""

def makeParser(parser):
    if not isinstance(parser, argparse.ArgumentParser):
        sys.exit("Please pass this method a valid instance of argparse.ArgumentParser")
    
    try:
        origInstanceName = config.get("global", "instance")
    except ConfigParser.Error as error:
        sys.exit("Configuration error in 'register' module:\n{0}".format(error))
    
    try:
        client = config.get("global", "client")
    except ConfigParser.NoOptionError as error:
        parser.add_argument(
            "-c",
            "--client",
            required = True,
            help = "The client to register or de-register the instance with (e.g. CARR).  This is required if there is no default in the configuration file."
        )
    except ConfigParser.NoSectionError as error:
        sys.exit("Configuration error:\n{0}".format(error))
    else:
        parser.add_argument(
            "-c",
            "--client",
            default = client,
            help = "The client to register or de-register the instance with (default: %(default)s).  This is used to override the default in the configuration file (i.e. for multi-client environments)."
        )
    
    parser.add_argument(
        "environment",
        help = "The environment to register or de-register the instance with."
    )
    parser.add_argument(
        "instanceName",
        metavar = "instance",
        default = origInstanceName,
        nargs = "?",
        help = "The name of this instance (default: %(default)s)."
    )
    parser.add_argument(
        "-x",
        "--deregister",
        dest = "action",
        action = "store_const",
        const = "deregister",
        default = "register",
        help = "Invoke this option to perform de-registration rather than registration."
    )
    parser.add_argument(
        "-u",
        "--jasperuser",
        default = "jasperadmin",
        help = "The name of the user that the REST workers should authenticate against the JasperReports Server with (default: %(default)s)."
    )
    parser.add_argument(
        "-p",
        "--jasperpassword",
        default = "jasperadmin",
        help = "The password of the user that the REST workers should authenticate against the JasperReports Server with (default: %(default)s)."
    )
    
    parser.set_defaults(func=parserWrapper)

def parserWrapper(func, moduleName, action, client, environment, instanceName, jasperuser, jasperpassword):
    try:
        register(action, client, environment, instanceName, jasperuser, jasperpassword)
    except RegistrationException as error:
        logging.getLogger("register").error(error)
    except Exception as error:
        logging.getLogger("register").error("An unexpected error occured:\n{0}".format(traceback.format_exc()))

def register(action, client, environment, instanceName, jasperuser, jasperpassword):
    if not all([action, instanceName]):
        raise RegistrationException("A null argument was provided for a required parameter.")
    if action not in ["register", "deregister"]:
        raise RegistrationException("An invalid 'action' argument was provided: {0}".format(action))
    
    try:
        ip = config.get("global", "ip")
        origInstanceName = config.get("global", "instance")
        connectionTimeout = config.get("register", "connectionTimeout")
    except ConfigParser.Error as error:
        raise RegistrationException("Configuration error:\n{0}".format(error))
    
    validateRegistry()
    
    if action == "register":
        logging.getLogger("register").info("Registering this instance as '{0}' at IP '{1}' in the {2}-{3} environment.".format(
            instanceName,
            ip,
            client.upper(),
            environment.upper(), 
        ))
    elif action == "deregister":
        logging.getLogger("register").info("De-registering this instance.")
    
    message = json.dumps({
        "action": action,
        "client": client,
        "environment": environment,
        "instanceType": "jasper",
        "instanceName": instanceName,
        "ip": ip
    })
    
    logging.getLogger("register").debug("Sending the following message with a routing key of 'cli.register': {0}".format(message))

    try:
        messageMaker = MessageMaker.MessageMaker(
            exchangeName = "teams",
            timeout = connectionTimeout
        )
    except MessageMaker.MessagingException as error:
        raise RegistrationException("Unable to create a MessageMaker object:\n{0}".format(error))
    try:
        response = messageMaker.send(
            routingKey = "cli.register",
            message = message,
            rpc = True,
            expectedResponses = 1
        )
    except MessageMaker.MessagingException as error:
        raise RegistrationException("Unable to contact the CLI server:\n{0}".format(error))
    finally:
        messageMaker.close()
    
    try:
        responseDict = json.loads(response)
    except Exception as error:
        raise RegistrationException("Unable to parse the JSON message:\n{0}\n{1}".format(
            response,
            error
        ))
    try:
        message = responseDict["message"]
        status = responseDict["status"]
        responseDetails = responseDict["details"]
    except KeyError as error:
        raise RegistrationException("Unable to parse the response from the CLI:\n{0}\n{1}".format(
            responseDict,
            error
        ))

    if status == "failure":
        raise RegistrationException(message)
    if len(responseDetails) != 1:
        raise RegistrationException("Unable to parse the response from the CLI:\n{0}".format(responseDetails))
    logging.getLogger("register").info(responseDetails[0]["message"])
    
    newInstanceName = None
    if action == "register" and origInstanceName != instanceName:
        logging.getLogger("register").info("Changing the instance name from '{0}' to '{1}' in the local configuration file.".format(
            origInstanceName,
            instanceName
        ))
        newInstanceName = instanceName
    elif action == "deregister":
        logging.getLogger("register").info("Clearing the instance name from from the local configuration file to reflect de-registration.")
        newInstanceName = ""
    if newInstanceName:
        config.set("global", "instance", newInstanceName)
        try:
            configFile = open("/etc/teams/jasper-workers", "w")
            config.write(configFile)
        except EnvironmentError as error:
            raise RegistrationException("Unable to write to the configation file at /etc/teams/jasper-workers:\n{0}".format(error))
    
    try:
        os.setegid(pwd.getpwnam("root").pw_gid)
        os.seteuid(pwd.getpwnam("root").pw_uid)
    except EnvironmentError as error:
        if error.errno == errno.EPERM:
            raise RegistrationException("Unable to change the user to 'root':\n{0}".format(error))
    
    logging.getLogger("register").debug("Determining the status of the teams-jasper-workers service")
    try:
        serviceStatus = execute.execute("status teams-jasper-workers", "register")
    except execute.ExecuteException as error:
        raise RegistrationException("Unable to determine the status of the teams-jasper-workers service:\n{0}".format(error))
    if any(phrase in serviceStatus for phrase in ["running", "started"]):
        logging.getLogger("register").info("Stopping the teams-jasper-workers service")
        try:
            serviceStatus = execute.execute("stop teams-jasper-workers", "register")
        except execute.ExecuteException as error:
            raise RegistrationException("Unable to stop the teams-jasper-workers service: {0}".format(error))
        else:
            logging.getLogger("register").debug("The teams-jasper-workers service is stopped")
    
    try:
        os.setegid(pwd.getpwnam("teams").pw_gid)
        os.seteuid(pwd.getpwnam("teams").pw_uid)
    except EnvironmentError as error:
        if error.errno == errno.EPERM:
            raise RegistrationException("Unable to change to the 'teams' user:\n{0}".format(error))

    
    if action == "register":
        logging.getLogger("register").info("Setting configuration on the CLI.")
        configure.configure(
            client=client, 
            environment=environment,
            option="jasperUser",
            value=jasperuser
        )
        configure.configure(
            client=client, 
            environment=environment,
            option="jasperpassword",
            value=jasperpassword
        )
        
        logging.getLogger("register").debug("Determining the status of the teams-jasper-workers service")
        
        try:
            os.setegid(pwd.getpwnam("root").pw_gid)
            os.seteuid(pwd.getpwnam("root").pw_uid)
        except EnvironmentError as error:
            if error.errno == errno.EPERM:
                raise RegistrationException("Unable to change the user to 'root':\n{0}".format(error))
        
        try:
            serviceStatus = execute.execute("status teams-jasper-workers", "register")
        except execute.ExecuteException as error:
            raise RegistrationException("Unable to determine the status of the teams-jasper-workers service:\n{0}".format(error))
        if any(phrase in serviceStatus for phrase in ["waiting", "not running", "stopped"]):
            logging.getLogger("register").info("Starting the teams-jasper-workers service")
            try:
                serviceStatus = execute.execute("start teams-jasper-workers", "register")
            except execute.ExecuteException as error:
                raise RegistrationException("Unable to start the teams-jasper-workers service:\n{0}".format(error))
            else:
                logging.getLogger("register").debug("The teams-jasper-workers service is already running.")
        
        try:
            os.setegid(pwd.getpwnam("teams").pw_gid)
            os.seteuid(pwd.getpwnam("teams").pw_uid)
        except EnvironmentError as error:
            if error.errno == errno.EPERM:
                raise RegistrationException("Unable to change to the 'teams' user:\n{0}".format(error))
        
    elif action == "deregister":
        logging.getLogger("register").info("Leaving the teams-jasper-workers service stopped, since this instance has been de-registered.")

def validateRegistry():
    try:
        ip = config.get("global", "ip")
        instanceName = config.get("global", "instance")
    except ConfigParser.Error as error:
        sys.exit("Configuration error:\n{0}".format(error))
    
    if not ip:
        logging.getLogger("register").warn("This instance's IP address has not been set in the local configuration file.")
    if not instanceName:
        logging.getLogger("register").warn("This instance's name has not been set in the local configuration file.")
    
    reportedIP = None
    reportedHostname = None
    try:
        reportedHostname = socket.getfqdn()
    except socket.gaierror as error:
        logging.getLogger("register").debug("Unable to derive the hostname from shell utilities.")
    if reportedHostname:
        try:
            reportedIP = socket.gethostbyname(reportedHostname)
        except socket.gaierror as error:
            logging.getLogger("register").debug("Unable to derive the ip from the hostname.")
    if not reportedIP:
        try:
            reportedIP = execute.execute("ifconfig | grep -i \"inet\" | grep -iv \"inet6\" | awk {'print $2'} | sed -ne 's/addr\:/ /p'", "register").split("\n")[0]
        except execute.ExecuteException as error:
            logging.getLogger("register").debug("Unable to derive the IP address from shell utilities.")
    if ip and (reportedIP or reportedHostname):
        if not (reportedIP == ip or reportedHostname == ip):
            logging.getLogger("register").warn("The configured IP address ({0}) does not match the address reproted by the shell ({1}).".format(
                ip,
                reportedIP
            ))
