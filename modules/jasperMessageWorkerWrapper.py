#!/usr/local/lib/teams/python/bin/python
import importlib
import os
import sys
import pwd
import traceback
import pkgutil
import ConfigParser
import logging
import pika
import time

time.sleep(2)

config = ConfigParser.SafeConfigParser(os.environ)
config.read("/home/jasper-workers")

try:
    pythonLibRoot = config.get("global", "pythonLibRoot")
    logRoot = config.get("global", "logRoot")
    instance = config.get("global", "instance").lower()
    restConsumer = config.getboolean("rest", "restConsumer")
except ConfigParser.Error as error:
    sys.exit("Configuration error:\n{0}".format(error))

sys.path.append(pythonLibRoot)
from shared import MessageWorker

consumerDetailsList = list()
consumerModuleNames = [
]
for moduleName in consumerModuleNames:
    module = importlib.import_module("jasper.{0}".format(moduleName), package=moduleName)
    try:
        consumerDetailsList.append(
            MessageWorker.ConsumerDetails(
                queueName = "jasper-{0}-{1}".format(instance, moduleName),
                routingKey = ".".join(["jasper", moduleName, instance]),
                messageHandlerName = "jasper.{0}".format(moduleName),
                exchangeName = "teams"
            )
        )
    except MessageWorker.MessagingException as error:
        pass
        #sys.exit("{0} - {1}".format(moduleName, error))

if restConsumer:
    try:
        consumerDetailsList.append(
            MessageWorker.ConsumerDetails(
                queueName = "jasper-{0}-REST".format(instance),
                routingKey = "jasper.rest",
                messageHandlerName = "jasper.rest",
                exchangeName = "teams"
            )
        )
    except MessageWorker.MessagingException as error:
        sys.exit("Jasper REST consumer - {1}".format(error))

try:
    worker = MessageWorker.MessageWorker(*consumerDetailsList)
    worker.start()
except KeyboardInterrupt:
    worker.stop()
except MessageWorker.MessagingException as error:
    sys.exit(error)
except pika.exceptions.AMQPConnectionError as error:
    sys.exit("Unable to connect to the AMQP server:\n{0}".format(error))
except (pika.exceptions.AuthenticationError, pika.exceptions.ProbableAuthenticationError) as error:
    sys.exit("AMQP Authentication error:\n{0}".format(error))
except Exception as error:
    sys.exit("Encountered an unexpected error:\n{0}".format(traceback.format_exc()))
